﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorChecker : MonoBehaviour
{
    static public EventHandler OnEnterWoodenFloor;
    static public EventHandler OnEnterConcreteFloor;
    static public EventHandler OnExitWoodenFloor;
    static public EventHandler OnExitConcreteFloor;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == ("Wooden"))
        {
            Debug.Log("Has Entered");
            OnEnterWoodenFloor?.Invoke(this, EventArgs.Empty);
        }
        else if(other.gameObject.tag == ("Concrete"))
        {
            OnEnterConcreteFloor?.Invoke(this, EventArgs.Empty);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == ("Wooden"))
        {
            Debug.Log("HasExited"); 
            OnExitWoodenFloor?.Invoke(this, EventArgs.Empty);
        }
        else if (other.gameObject.tag == ("Concrete"))
        {
            OnExitConcreteFloor?.Invoke(this, EventArgs.Empty);
        }
    }




}
