﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heartbeat : MonoBehaviour
{
    float distance;

    public Enemy enemy;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(transform.position, enemy.transform.position);

        if (distance < 70)
        {
            audioSource.volume += Time.deltaTime;
        }
        else
        {
            audioSource.volume -= Time.deltaTime;
        }
    }

    
}
