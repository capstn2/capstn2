﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public int keyIDNeeded;

    public float turnSmoothTime;
    float turnSmoothVelocity;

    public Vector3 newPos;
    private Vector3 origPos;

    public ObjectiveSlot slot;

    public bool isOpen = false;
    void Start()
    {
        origPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(isOpen)
        {
            float posX = Mathf.SmoothDampAngle(this.transform.position.x, 549f ,ref turnSmoothVelocity, turnSmoothTime);
            transform.position = new Vector3(posX, this.transform.position.y, this.transform.position.z);
        }
    }

    public void CheckIfYouHaveKey(Inventory inv)
    {
        for (int i = 0; i < inv.GetInventoryList().Count; i++)
        {
            if (inv.GetInventoryList()[i].interactable.itemType == Item.ItemType.Key)
            {
                if (inv.GetInventoryList()[i].interactable.keyID == keyIDNeeded)
                {
                    isOpen = true;
                    return;
                }
            }
        }
        ObjectiveHandler.instance.AddObjective(slot);
    }
}
