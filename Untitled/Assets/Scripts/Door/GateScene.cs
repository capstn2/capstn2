﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GateScene : MonoBehaviour
{
    public string sceneName;
    void Start()
    {
        
    }

    void Update()
    {
    }

    public void ChangeScene()
    {
        if(SceneManagerScript.instance.ReturnSceneName() == "Aswang")
        {
            Debug.Log("Survived Aswang");
            SaveManager.instance.stageData.survivedAswang = true;
            ObjectiveEvent.Instance.OnQuestArrivedS(4321);
        }
        else if (SceneManagerScript.instance.ReturnSceneName() == "Manananggal")
        {
            Debug.Log("Survived Manananggal");
            SaveManager.instance.stageData.survivedManananggal = true;
            ObjectiveEvent.Instance.OnQuestArrivedS(9876);
        }
        SceneManagerScript.instance.ChangeSceneBaseOnString(sceneName);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (SceneManagerScript.instance.ReturnSceneName() == "Aswang")
            {
                Debug.Log("Survived Aswang");
                SaveManager.instance.stageData.survivedAswang = true;
                ObjectiveEvent.Instance.OnQuestArrivedS(4321);
            }
            else if (SceneManagerScript.instance.ReturnSceneName() == "Manananggal")
            {
                Debug.Log("Survived Manananggal");
                SaveManager.instance.stageData.survivedManananggal = true;
                ObjectiveEvent.Instance.OnQuestArrivedS(9876);
            }
            SceneManagerScript.instance.ChangeSceneBaseOnString(sceneName);
        }
    }
}
