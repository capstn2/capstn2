﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CollectBehaviour : MonoBehaviour
{
    public Transform iOrigin;
    public LayerMask noteMask;

    public event EventHandler OnItemDetect;

    [SerializeField] private UI_Inventory uiInventory;

    private Inventory inventory;
    private float interactRange = 5f;
    private Transform itemRayCast;
    private Transform itemPrompt;
    private ThirdPersonMovement playerMovement;

    private ItemIndicatorScript itemIndi;
    private bool nullChecked = false;
    private float nullTimer = .5f;



    private void Awake()
    {

        inventory = new Inventory(UseItem);
        //inventory = new Inventory();
        uiInventory.Hide();
        itemRayCast = transform.Find("itemRayCast");
        itemPrompt = transform.Find("itemPrompt");
        uiInventory.SetPlayer(this);
        uiInventory.SetInventory(inventory);
        playerMovement = GetComponent<ThirdPersonMovement>();
        Referencer.Instance.colBeh = this;
        //ItemWorld.SpawnItemWorld(new Vector3(20, 0, 20), new ItemREDO { itemType = ItemREDO.ItemType.Ash, amount = 1 });
        //ItemWorld.SpawnItemWorld(new Vector3(-20, 0, 20), new ItemREDO { itemType = ItemREDO.ItemType.Salt, amount = 1 });
        //ItemWorld.SpawnItemWorld(new Vector3(0, 0, -20), new ItemREDO { itemType = ItemREDO.ItemType.Garlic, amount = 1 });
    }

    private void Start()
    {
        //interactableTypes = inventory.GetInventoryList();
       
    }

    public void ReplaceInventory(Inventory inv)
    {
        inventory = inv;
    }

    // Update is called once per frame
    void Update()
    {
        //To prevent the null reference from showing in the build
        if(nullChecked == false)
        {
            uiInventory.Show();
            nullTimer -= Time.deltaTime;
            if(nullTimer <= 0)
            {
                uiInventory.Hide();
                nullChecked = true;
            }

        }// --
        /*
        if (Input.GetKeyDown(KeyCode.I))
        {
            if(uiInventory.GetIsOpen() == true)
            {
                uiInventory.Hide();
                uiInventory.SetIsOpen(false);
                playerMovement.canMove = true;
                Time.timeScale = 1;
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else if(uiInventory.GetIsOpen() == false)
            {
                uiInventory.Show();
                uiInventory.SetIsOpen(true);
                playerMovement.canMove = false;
                Time.timeScale = 0;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;


            }

        }*/
        Debug.DrawRay(iOrigin.transform.position, Camera.main.transform.forward *10, Color.green);

        RaycastHit prompt;
       
        if (Physics.Raycast(new Vector3(itemRayCast.position.x, itemRayCast.position.y, itemRayCast.position.z), itemRayCast.forward, out prompt, interactRange))
        {
            Debug.DrawRay(itemRayCast.position, transform.forward, Color.red, 30f);
            ItemWorld interactable = prompt.collider.GetComponent<ItemWorld>();
            if (interactable != null)
            {
                ItemIndicatorScript indi = interactable.transform.GetChild(0).gameObject.GetComponent<ItemIndicatorScript>();

               // var text = itemPrompt.Find("text");
                var item = interactable.GetComponent<InteractableTypeHolder>();
               // itemPrompt.gameObject.SetActive(true);
               // text.GetComponent<TextMesh>().text = "E - " + item.interactableType.nameString;

                indi.canGrabItem = true;
                itemIndi = indi;

                ItemPromtUI.Instance.Show("E - " + item.interactableType.nameString);
            }
            else if (itemIndi != null && interactable == null)
            {
                itemIndi.canGrabItem = false;
                //itemPrompt.gameObject.SetActive(false);

                ItemPromtUI.Instance.Hide();

            }
        }
        else
        {
            ItemPromtUI.Instance.Hide();
        }

        RaycastHit interactHit;
        if (Physics.Raycast(iOrigin.transform.position, Camera.main.transform.forward, out interactHit, 6, noteMask))
        {
            if (interactHit.collider.GetComponent<NoteItem>() != null) 
            {
                InteractablePromptUI.Instance.Show(interactHit.collider.GetComponent<NoteItem>().noteToGive.noteButTitle + " - E ");
            }
            else if (interactHit.collider.GetComponent<Gate>() != null)
            {
                InteractablePromptUI.Instance.Show("Open Gate"+" - E ");
            }
            else if (interactHit.collider.GetComponent<Ladder>() != null)
            {
                InteractablePromptUI.Instance.Show("Use Ladder" + " - E ");
            }
            else if (interactHit.collider.GetComponent<Corpse>() != null)
            {
                InteractablePromptUI.Instance.Show("Bless Corpse" + " - E ");
            }
            else if (interactHit.collider.GetComponent<LadderInteract>() != null)
            {
                InteractablePromptUI.Instance.Show("PlaceLadder" + " - E ");
            }
        }
        else
        {
            InteractablePromptUI.Instance.Hide();
        }

        /*
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Trying to Get Item");
            Debug.DrawRay(transform.position, transform.forward, Color.red, 30f);
            RaycastHit hit;
            if (Physics.Raycast(new Vector3(itemRayCast.position.x, itemRayCast.position.y, itemRayCast.position.z), itemRayCast.forward, out hit, interactRange))
            {
                Debug.Log("hit something");
                ItemWorld interactable = hit.collider.GetComponent<ItemWorld>();

                if (interactable)
                {
                    GameObject pickUpObject = interactable.gameObject;
                }
                if(interactable != null)
                {
                    inventory.AddItem(interactable.GetItem());
                    var interactableType = interactable.GetComponent<InteractableTypeHolder>().interactableType;

                    //------Test-----
                    if (interactableType.questItem)
                    {
                        var playerInteract = GetComponent<PlayerInteract>();
                        switch (interactableType.itemType)
                        {
                            case Item.ItemType.Ash:
                                playerInteract.SetAsh(true);
                                break;
                            case Item.ItemType.Garlic:
                                playerInteract.SetGarlic(true);
                                break;
                            case Item.ItemType.Salt:
                                playerInteract.SetSalt(true);
                                break;
                        }
                    }
                    //------Test-----

                    //itemPrompt.gameObject.SetActive(false);
                    ItemPromtUI.Instance.Hide();
                    interactable.DestroySelf();



                    Destroy(interactable.gameObject);
                }

                if (hit.collider.GetComponent<EnemyInteract>())
                {
                    if (GetComponent<PlayerInteract>().hasAll())
                    {
                        //OnCollectAll?.Invoke(this, EventArgs.Empty);
                        hit.collider.GetComponent<EnemyInteract>().upperBody.GetComponent<Enemy>().SetCollectAllItem(true);
                    }
                }

            }

        }*/

    }

    public void InteractItem()
    {
        //Debug.Log("Trying to Get Item");
        Debug.DrawRay(transform.position, transform.forward, Color.red, 30f);
        RaycastHit hit;
        if (Physics.Raycast(new Vector3(itemRayCast.position.x, itemRayCast.position.y, itemRayCast.position.z), itemRayCast.forward, out hit, interactRange))
        {
            
            ItemWorld interactable = hit.collider.GetComponent<ItemWorld>();

            if (interactable)
            {
                GameObject pickUpObject = interactable.gameObject;
            }
            if (interactable != null)
            {
                inventory.AddItem(interactable.GetItem());
                //For Checking if quest needed this item
                ObjectiveEvent.Instance.OnCheckItemType(interactable.GetItem().interactable.itemType);
                var interactableType = interactable.GetComponent<InteractableTypeHolder>().interactableType;

                //------Test-----
                if (interactableType.questItem)
                {
                    var playerInteract = GetComponent<PlayerInteract>();
                    switch (interactableType.itemType)
                    {
                        case Item.ItemType.Ash:
                            playerInteract.SetAsh(true);
                            break;
                        case Item.ItemType.Garlic:
                            playerInteract.SetGarlic(true);
                            break;
                        case Item.ItemType.Salt:
                            playerInteract.SetSalt(true);
                            break;
                    }
                }
                //------Test-----

                //itemPrompt.gameObject.SetActive(false);
                ItemPromtUI.Instance.Hide();
                interactable.DestroySelf();

                Destroy(interactable.gameObject);
            }



            if (hit.collider.GetComponent<EnemyInteract>())
            {
                if (GetComponent<PlayerInteract>().hasAll())
                {
                    Debug.Log("Collected All");
                    //OnCollectAll?.Invoke(this, EventArgs.Empty);
                    hit.collider.GetComponent<EnemyInteract>().upperBody.GetComponent<Enemy>().SetCollectAllItem(true);
                    ObjectiveEvent.Instance.OnQuestArrivedS(5463);
                }
            }
 
        }
        OtherInteract();
        
    }

    public void OtherInteract()
    {
        RaycastHit hit;
        if (Physics.Raycast(iOrigin.transform.position, Camera.main.transform.forward, out hit, 100, noteMask))
        {
            Debug.Log("Hit : " + hit.collider.gameObject.name);
            if (hit.collider.GetComponent<NoteItem>())
            {
                NoteItem np = hit.collider.GetComponent<NoteItem>();
                np.AddNoteToPlayer(this.gameObject);
            }
            if (hit.transform.GetComponent<Gate>() != null)
            {
                Debug.Log("GateDetected");
                hit.transform.GetComponent<Gate>().CheckIfYouHaveKey(GetInventory());
            }

            if (hit.transform.GetComponent<Ladder>() != null)
            {
                hit.transform.GetComponent<Ladder>().InteractLadder(this.gameObject.transform);
            }

            if (hit.transform.GetComponent<Corpse>() != null)
            {
                hit.transform.GetComponent<Corpse>().BlessTheCorpse(inventory);
            }

            if (hit.transform.GetComponent<LadderInteract>() != null)
            {
                hit.transform.GetComponent<LadderInteract>().PlaceLadder();
            }
        }
    }

    public void ToggleInventory()
    {
        if (uiInventory.GetIsOpen() == true)
        {
            uiInventory.Hide();
            uiInventory.SetIsOpen(false);
            playerMovement.canMove = true;
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (uiInventory.GetIsOpen() == false)
        {
            uiInventory.Show();
            uiInventory.SetIsOpen(true);
            playerMovement.canMove = false;
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void UseItem(Item item)
    {
        switch (item.interactable.itemType)
        {
            case Item.ItemType.Salt:
                Debug.Log("Use salt");
                //inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;

            case Item.ItemType.Ash:
                Debug.Log("Use Ash");
                //inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;

            case Item.ItemType.Garlic:
                Debug.Log("Use Garlic");
                //inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;

            case Item.ItemType.Herb:
                Debug.Log("Use HealthPotion");
                GetComponent<HealthScript>().HealUnit(50);
                inventory.RemoveItem(new Item { interactable = item.interactable, amount = 1 });
                break;
        }
    }


    private void TestHide()
    {
        ItemPromtUI.Instance.Hide();
    }

    public UI_Inventory ReturnUIInventory()
    {
        return uiInventory;
    }

    public Inventory GetInventory()
    {
        return inventory;
    }

}
