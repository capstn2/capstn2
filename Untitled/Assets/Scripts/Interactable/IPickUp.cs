﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPickUp 
{
    void PickUpObject();
    void DropObject();
    void ThrowObject();
}
