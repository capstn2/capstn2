﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemWorldSpawner : MonoBehaviour
{
    public Item item;
    public GameObject itemIndicator;

    public bool shouldSpawn = true;
    private void Awake()
    {
        
    }
    private void Start()
    {
        if (shouldSpawn)
        {
            ItemWorld.SpawnItemWorld(transform.position, item);
        }
        Destroy(this.gameObject);

    }
}
