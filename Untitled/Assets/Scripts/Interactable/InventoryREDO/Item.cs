﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Item
{

    public enum ItemType
    {
        Salt,
        Ash,
        Garlic,
        HealthPotion,
        BottleOfOil,
        Amulet,
        BlowpipeAmmo,
        Battery,
        FireballScroll,
        FreezeScroll,
        HealScroll,
        LightScroll,
        Chalk,
        AlawigEssence,
        TalismanPiece,
        Herb,
        Coal,
        Key,
        Weapon,
        TearsOfBathala
    }

    public InteractableTypeSO interactable;
    public int amount;

    public Sprite GetSprite()
    {
        return interactable.sprite2D;
    }

    public Item ReturnDeepCopy()
    {
        Item dItem = new Item();
        dItem.interactable = this.interactable;
        dItem.amount = this.amount;
        return dItem;
    }
    
}
