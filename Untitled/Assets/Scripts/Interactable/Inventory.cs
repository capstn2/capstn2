﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory
{
    public event EventHandler OnItemListChanged;

    private List<Item> itemList = new List<Item>();
    private Action<Item> useItemAction;

    
    public Inventory(Action<Item> useItemAction)
    {
        this.useItemAction = useItemAction;
        itemList = new List<Item>();


        Debug.Log("Inventory");
    }

    public void AddItem(Item item)
    {
        if (item.interactable.isStackable)
        {
            bool itemAlreadyInInventory = false;
            foreach(Item inventoryItem in itemList)
            {
                if(inventoryItem.interactable.itemType == item.interactable.itemType)
                {
                    inventoryItem.amount++;
                    itemAlreadyInInventory = true;
                }
            }
            if (!itemAlreadyInInventory)
            {
                itemList.Add(item);
            }
        }
        else
        {
            itemList.Add(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveItem(Item item)
    {
        if (item.interactable.isStackable)
        {
            Item itemInInventory = null;
            foreach(Item inventoryItem in itemList)
            {
                if(inventoryItem.interactable.itemType == item.interactable.itemType)
                {
                    inventoryItem.amount--;
                    itemInInventory = inventoryItem;
                }
            }
            if(itemInInventory != null && itemInInventory.amount <= 0)
            {
                itemList.Remove(itemInInventory);
            }
        }
        else
        {
            itemList.Remove(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }


    public void UseItem(Item item)
    {
        useItemAction(item);
    }


    public List<Item> GetInventoryList()
    {
        return itemList;
    }

    public void ChangeInvItemList(List<Item> iList)
    {
        itemList = iList;
    }

}
