﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class ItemWorld : MonoBehaviour
{
    
    public static ItemWorld SpawnItemWorld(Vector3 position, Item item)//Change something by pao; , GameObject itemIndicator
    {
        Transform transform = Instantiate(item.interactable.prefab, position, Quaternion.identity);
        //GameObject indicator = Instantiate(itemIndicator, transform.position, transform.rotation, transform);
        //indicator.GetComponent<ItemIndicatorScript>().item = transform.gameObject;
        ItemWorld itemWorld = transform.GetComponent<ItemWorld>();
        itemWorld.SetItem(item);

        return itemWorld;
    }


    public static ItemWorld DropItem(Vector3 dropPosition, Item item) //, GameObject itemIndi
    {
        Vector3 randomDir = UtilsClass.GetRandomDir();
        ItemWorld itemWorld = SpawnItemWorld(dropPosition + randomDir * 5, item); //,itemIndi
        itemWorld.GetComponent<Rigidbody>().AddForce(randomDir * 5, ForceMode.Impulse);
        return itemWorld;
    }

    private Item item;




    public void SetItem(Item item)
    {
        this.item = item;
        item.amount = 1;

    }

    public Item GetItem()
    {
        if (this.GetComponent<ManualEventScript>() != null)
        {
            GameObject ins = GameObject.FindGameObjectWithTag("Instruction3");
            if (ins != null)
            {
                ins.GetComponent<InstructionScript>().DestroyInstruction();
            }
            GameObject ins2 = GameObject.FindGameObjectWithTag("Instruction4");
            ins2.GetComponent<InstructionScript>().MakeIsActive();
            ins2.GetComponent<InstructionScript>().DisplayInstruction();
        }
        return item;
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
