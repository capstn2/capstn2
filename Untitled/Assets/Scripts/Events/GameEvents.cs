﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : Singleton<GameEvents>
{
    public event Action radarMakesNoise;

    public event Action onSave;
    public event Action onLoad;
    public event Action clearListeners;

    public void ClearListeners()
    {
        if (clearListeners != null)
        {
            clearListeners();
        }
    }

    public void OnSave()
    {
        if (onSave != null)
        {
            onSave();
        }
    }

    public void OnLoad()
    {
        if (onLoad != null)
        {
            onLoad();
        }
    }

    public void RadarMakesNoise()
    {
        if (radarMakesNoise != null)
        {
            radarMakesNoise();
        }
    }
}
