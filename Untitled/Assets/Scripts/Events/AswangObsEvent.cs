﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AswangObsEvent : Singleton<AswangObsEvent>
{
    //public event Action onMananangalSeePlayerLH;

    //public delegate void OnManananggalSeePlayerLowH(AswangObservation aswObs);
    public event Action onManananggalSeePlayerLH;

    //public delegate void OnPlayerSeeManananggalNoLow(AswangObservation aswObs);
    public event Action onPlayerSeeManananggalNoLB;

    //public delegate void OnPlayerDiscoverManananggalW(AswangObservation aswObs);
    public event Action onPlayerSeeManananggalWeakN;

    public event Action onPlayerSawAswangInvi;

    public event Action onPlayerEnterFamilyEstate;

    public event Action onPlayerSeeAswangEatCorpse;

    public event Action onPlayerDiscoverWeakness;

    public void OnManananggalSeePlayerLowHealth()
    {
        if (onManananggalSeePlayerLH != null)
        {
            onManananggalSeePlayerLH();
        }
    }

    public void OnPlayerSeeManananggalNoLowerBody()
    {
        if (onPlayerSeeManananggalNoLB != null)
        {
            onPlayerSeeManananggalNoLB();
        }
    }

    public void OnPlayerDiscoverManananggalWeakness()
    {
        if (onPlayerSeeManananggalWeakN != null)
        {
            onPlayerSeeManananggalWeakN();
        }
    }

    public void OnPlayerSawAswangInvisible()
    {
        if(onPlayerSawAswangInvi !=null)
        {
            onPlayerSawAswangInvi();
        }
    }

    public void OnPlayerEnterFamilyEstate()
    {
        if (onPlayerEnterFamilyEstate != null)
        {
            onPlayerEnterFamilyEstate();
        }
    }

    public void OnPlayerSeeAswangEatingCorpses()
    {
        if (onPlayerSeeAswangEatCorpse != null)
        {
            onPlayerSeeAswangEatCorpse();
        }
    }

    public void OnPlayerDiscoveredAswangWeakness()
    {
        if (onPlayerDiscoverWeakness != null)
        {
            onPlayerDiscoverWeakness();
        }
    }

}
