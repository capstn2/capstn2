﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatEvents : Singleton<CombatEvents>
{
    public event Action onPlayerSave;

    public void OnPlayerSave()
    {
        if (onPlayerSave != null)
        {
            onPlayerSave();
        }
    }
}
