﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ladder : MonoBehaviour
{
    public Transform destinedPos;
    private Transform lPlayer;

    public GameObject blocker;

    public float fadeDuration;
    private float defDuration;
    private bool canBeInteracted = true;
    private float alp = 0;
    void Start()
    {
        defDuration = fadeDuration;
    }

    void Update()
    {
        
    }

    public void InteractLadder(Transform player)
    {
        if (canBeInteracted)
        {
            lPlayer = player;
            canBeInteracted = false;
            StartCoroutine("FadeOut");
        }
    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(Time.deltaTime);
        alp += Time.deltaTime/ defDuration;
        Color col = new Color(0, 0, 0, alp);
        blocker.GetComponent<Image>().color = col;
        if(fadeDuration<=0)
        {
            alp = 1;
            fadeDuration = defDuration;
            StartCoroutine("TransferPlayer");
        }
        else
        {
            fadeDuration -= Time.deltaTime;
            StartCoroutine("FadeOut");
        }
    }
    IEnumerator TransferPlayer()
    {
        lPlayer.transform.position = destinedPos.position;
        yield return new WaitForSeconds(0.5f);
        StartCoroutine("FadeIn");
    }
    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(Time.deltaTime);
        alp -= Time.deltaTime / defDuration;
        Color col = new Color(0, 0, 0, alp);
        blocker.GetComponent<Image>().color = col;
        if (fadeDuration <= 0)
        {
            alp = 0;
            fadeDuration = defDuration;
            canBeInteracted = true;
            lPlayer = null;
        }
        else
        {
            fadeDuration -= Time.deltaTime;
            StartCoroutine("FadeIn");
        }
    }
}
