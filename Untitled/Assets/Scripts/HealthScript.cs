﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : MonoBehaviour
{
    public float curHealth;
    public float maxHealth;

    [SerializeField] AudioClip[] painClip;
    [SerializeField] AudioSource audioSource;

    //-Manananggal Grip Damage variable--//
    [SerializeField] private bool onGrip = false;
    private bool gripActive = false;
    private int gripTick = 0;

    private bool onBite = false;
    private bool biteActive = false;
    private int biteTick = 0;

    //-Magnkukulam Fire Damage variable--//
    private bool onFire = false;
    private bool isActive = false;
    private int fireTick = 0;

    private IEnumerator burnCoroutine;
    private IEnumerator gripCoroutine;
    private IEnumerator biteCoroutine;

    private void Awake()
    {
        burnCoroutine = BurnDamage(5f);
        gripCoroutine = GripDamage(30f);
        biteCoroutine = BiteDamage(40f);


    }
    // Start is called before the first frame update
    void Start()
    {
        //curHealth = maxHealth;
        Attack.OnMangkukulamAttack += Attack_OnMangkukulamAttack;
        SpecialSkill.OnManananggalSpecial += SpecialSkill_OnManananggalSpecial;
        SpecialSkill.OnAswangSpecial += SpecialSkill_OnAswangSpecial;
        ButtonMashControl.OnButtonMashFinished += ButtonMashControl_OnButtonMashFinished;
    }

    void ButtonMashControl_OnButtonMashFinished(object sender, EventArgs e)
    {
        StopCoroutine(gripCoroutine);
        onGrip = false;
        gripActive = false;
        gripTick = 0;
    }


    void SpecialSkill_OnAswangSpecial(object sender, EventArgs e)
    {
        onBite = true;
    }


    void SpecialSkill_OnManananggalSpecial(object sender, System.EventArgs e)
    {
        onGrip = true;
    }


    void Attack_OnMangkukulamAttack(object sender, System.EventArgs e)
    {
        onFire = true;

        //StartCoroutine(coroutine);
    }


    // Update is called once per frame
    void Update()
    {
        #region Manananggal grip damage to player;
        if (onGrip)
        {

            if (!gripActive)
            {


                gripActive = true;
                StartCoroutine(gripCoroutine);
            }
        }

        if(gripTick == 5)
        {
            StopCoroutine(gripCoroutine);
            onGrip = false;
            gripActive = false;
            gripTick = 0;
        }

        #endregion


        #region Mangkukulam burning damage to player;
        if (onFire)
        {
            if (!isActive)
            {
                isActive = true;
                StartCoroutine(burnCoroutine);
            }

        }

        if (fireTick == 4)
        {

            StopCoroutine(burnCoroutine);
            onFire = false;
            isActive = false;
            fireTick = 0;
        }
        #endregion

        #region Aswang biting player
        if (onBite)
        {
            if (!biteActive)
            {
                biteActive = true;
                StartCoroutine(biteCoroutine);
            }
        }

        if(biteTick == 4)
        {
            StopCoroutine(biteCoroutine);
            onBite = false;
            biteActive = false;
            biteTick = 0;
        }
        #endregion



        //if (Input.GetKeyUp(KeyCode.Escape))
        //{
        //    Death();
        //}
    }  

    private IEnumerator BurnDamage(float damage)
    {
        while (true)
        {
            Debug.Log("Burning Player");
            DamageUnit(damage);

            yield return new WaitForSeconds(2);
            Debug.Log("Fire Tick");
            Debug.Log(fireTick);

            fireTick++;
        }
       
    }

    private IEnumerator GripDamage(float damage)
    {
        while (true)
        {
            Debug.Log("Gripping Player");
            DamageUnit(damage);

            yield return new WaitForSeconds(2);

            Debug.Log("Grip Tick");

            gripTick++;
        }
    }

    private IEnumerator BiteDamage(float damage)
    {
        while (true)
        {
            Debug.Log("Biting Player");
            DamageUnit(damage);

            yield return new WaitForSeconds(1);

            Debug.Log("Bite Tick");

            biteTick++;
        }
    }

    public void DamageUnit(float damage)
    {
        AudioClip clip = GetRandomClipPain();
        audioSource.clip = clip;
        audioSource.PlayOneShot(clip);
        curHealth -= damage;
        if(curHealth<0)
        {
            curHealth = 0;
            Death();
        }
    }

    public void HealUnit(float heal)
    {
        curHealth += heal;
        if(curHealth>maxHealth)
        {
            curHealth = maxHealth;
        }
    }

    public void Death()
    {
        if(this.gameObject.tag == "Player")
        {
            this.GetComponent<PlayersData>().SavePlayerInventory();
            this.GetComponent<PlayersData>().SavePlayerNotesAndObservations();
            GameManager.instance.OnPlayerDeath(this.gameObject);
        }
    }

    public bool GetGrip()
    {
        return onGrip;
    }

    public bool GetBite()
    {
        return onBite;
    }

    private AudioClip GetRandomClipPain()
    {
        return painClip[UnityEngine.Random.Range(0, painClip.Length)];
    }


}
