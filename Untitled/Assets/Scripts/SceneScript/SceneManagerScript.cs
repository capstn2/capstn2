﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerScript : MonoBehaviour
{
    public static SceneManagerScript instance;

    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //instance = this;
    }
    void Start()
    {
        GameEvents.Instance.onSave += CheckScene;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            ChangeSceneToDay();
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            ChangeSceneToNight();
        }*/
    }

    public void ChangeSceneToDay()
    {
        SaveManager.instance.Save();
        SaveManager.instance.ClearListeners();
        SaveManager.instance.playerData.loadAllData = false;
        SceneManager.LoadScene("AlphaBuild_Day");
    }
    public void ChangeSceneToNight()
    {
        SaveManager.instance.Save();
        SaveManager.instance.ClearListeners();
        SaveManager.instance.playerData.loadAllData = false;
        SceneManager.LoadScene(0);
    }

    public void ChangeSceneBaseOnString(string sceneName)
    {
        SaveManager.instance.Save();
        SaveManager.instance.ClearListeners();
        SceneManager.LoadScene(sceneName);
    }

    public string ReturnSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void CheckScene()
    {
        for(int i = 0; i< SaveManager.instance.activeSave.stageData.Count;i++)
        {
            if(SceneManager.GetActiveScene().name == SaveManager.instance.activeSave.stageData[i].stageName)
            {
                Debug.Log("HasSameScene");
                SaveManager.instance.hasLoaded = true;
                return;
            }
        }
        string sceneName = SceneManager.GetActiveScene().name;
        StageData data = new StageData();
        data.stageName = sceneName;
        SaveManager.instance.activeSave.stageData.Add(data);
    }
    public void RestartScene()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
