﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;

    public SaveData activeSave;

    public PlayerSaveData playerData;
    public StageSaveData stageData;
    public DaySequenceSaveData daySequenceData;

    public bool hasLoaded;

    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        //Load();
    }

    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Keypad7))
        {
            Save();
        }

        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            Load();
        }

        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            DeleteSave();
        }
    }

    public void Save()
    {
        /*
        string dataPath = Application.persistentDataPath;
        
        var serializer = new XmlSerializer(typeof(SaveData));
        var stream = new FileStream(dataPath + "/" + activeSave.saveName + ".save", FileMode.Create);
        serializer.Serialize(stream, activeSave);
        stream.Close();
        */
        //Debug.Log("SAved Int Count"+ activeSave.stageData[0].dPlayer.sItemList.Count);
        GameEvents.Instance.OnSave();
        //Debug.Log("Saved");

    }
    public void Load()
    {
        GameEvents.Instance.OnLoad();
        /*
        string dataPath = Application.persistentDataPath;

        if(System.IO.File.Exists(dataPath + "/" + activeSave.saveName + ".save"))
        {
            var serializer = new XmlSerializer(typeof(SaveData));
            var stream = new FileStream(dataPath + "/" + activeSave.saveName + ".save", FileMode.Open);
            activeSave = serializer.Deserialize(stream) as SaveData;
            stream.Close();

            //Debug.Log("Loaded Inventory" + activeSave.stageData[0].dPlayer.sItemList.Count);
            Debug.Log("Loaded");
            hasLoaded = true;
        }*/


    }

    public void ClearListeners()
    {

        GameEvents.Instance.ClearListeners();
    }

    public void DeleteSave()
    {
        /*
        string dataPath = Application.persistentDataPath;

        if (System.IO.File.Exists(dataPath + "/" + activeSave.saveName + ".save"))
        {
            File.Delete(dataPath + "/" + activeSave.saveName + ".save");
            Debug.Log("Deleted");
        }
        */

    }
}


[System.Serializable]
public class PlayerSaveData
{
    public Vector3 lastSavePostion;
    public Vector3 lastSaveRotation;

    public bool loadAllData = false;

    public List<Item> itemList;
    public List<PlayerNotes> playerNotes;
    public List<Objective> playerObjective;
    public List<AswangObservation> mananananggalObs;
    public List<AswangObservation> aswangObs;

    public AswangInfo aswang; 
    public bool discoveredAswang;
    public AswangInfo manananggal;
    public bool discoveredMananangal;
    public AswangInfo mangkukulam;
    public bool discoveredMangkukulam;
}

[System.Serializable]
public class StageSaveData
{
    public int numberOfDays = 0;

    public bool survivedManananggal = false;
    public bool scoutedTheAswangArea = false;
    public bool investigatedBloodTrails = false;
    public bool survivedAswang = false;
    public bool blessedTheGraves = false;
    public bool survivedMangkukulam = false;
    public bool hasLadder = false;
    public bool placedLadder = false;
}

[System.Serializable]
public class DaySequenceSaveData
{
    public bool talkedToGarcia = false;
    public bool talkedToMendoza = false;
    public bool checkedTravelHouse = false;
}


/// <summary>
/// ////////////////////////////////////////////////////////////////////////////////////
/// </summary>


[System.Serializable]
public class SaveData
{
    public string saveName;

    public List<StageData> stageData;
    public List<DataTypeNPC> dNPC = new List<DataTypeNPC>();
    public List<DataTypeObject> dObject = new List<DataTypeObject>();
}

[System.Serializable]
public class StageData
{
    public string stageName;

    public List<DataTypeNPC> dNPC = new List<DataTypeNPC>();
    public List<DataTypeObject> dObject = new List<DataTypeObject>();
}


[System.Serializable]
public class DataTypeNPC
{
    public string nName;
    public int dialIndex;
    public Vector3 npcPosition;
}

[System.Serializable]
public class DataTypeObject
{
    public string oName;
    public bool hasBeenCollected;
    public Vector3 objectPosition;

}

public enum TypeOfData
{
    Player,
    NPC,
    Object
}

