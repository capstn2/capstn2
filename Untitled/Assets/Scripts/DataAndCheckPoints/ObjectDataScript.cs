﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectDataScript : MonoBehaviour
{
    public TypeOfData thisObject;

    public bool hasData;

    void Start()
    {
        switch (thisObject)
        {
            case TypeOfData.Player:
                LoadPlayer();
                GameEvents.Instance.onSave += SavePlayer;
                GameEvents.Instance.onLoad += LoadPlayer;
                GameEvents.Instance.clearListeners += ClearThisListeners;
                break;
            case TypeOfData.NPC:
                //GameEvents.Instance.onSave += SaveNPC;
                //GameEvents.Instance.onLoad += LoadNPC;
                break;
            case TypeOfData.Object:
                //GameEvents.Instance.onSave += SaveObject;
                //GameEvents.Instance.onLoad += LoadObject;
                break;
        }
    
        
    }

    private void Update()
    {
    }
    
    public void ClearThisListeners()
    {
        GameEvents.Instance.onSave -= SavePlayer;
        GameEvents.Instance.onLoad -= LoadPlayer;
        GameEvents.Instance.clearListeners -= ClearThisListeners;
    }

    public void LoadPlayer()
    {
        if (SaveManager.instance.playerData.loadAllData)
        {
            //Load Position
            this.gameObject.transform.position = SaveManager.instance.playerData.lastSavePostion;
            this.gameObject.transform.eulerAngles = SaveManager.instance.playerData.lastSaveRotation;
        }
        //Load Inventory
        CollectBehaviour colBeh = this.GetComponent<CollectBehaviour>();
        //Copy Items from save
        List<Item> itemListCopyFSave = new List<Item>();
        for (int c = 0; c < SaveManager.instance.playerData.itemList.Count; c++)
        {
            itemListCopyFSave.Add(SaveManager.instance.playerData.itemList[c].ReturnDeepCopy());
        }
        colBeh.GetInventory().ChangeInvItemList(itemListCopyFSave);
        //////////////////
        ///Load PlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesLoad = new List<PlayerNotes>();
        for (int n = 0; n < SaveManager.instance.playerData.playerNotes.Count; n++)
        {
            pNotesLoad.Add(SaveManager.instance.playerData.playerNotes[n].ReturnDeepCopyNotes());
        }
        pData.pNotes = pNotesLoad;
        /////////////////
        ///
        Debug.Log("PlayerLoaded");
    }

    public void SavePlayer()
    {
        //
        SaveManager.instance.playerData.loadAllData = true;
        //Save Position
        SaveManager.instance.playerData.lastSavePostion = this.gameObject.transform.position;
        SaveManager.instance.playerData.lastSaveRotation = this.gameObject.transform.eulerAngles;
        //Getting Inventory.
        //Save Inventory
        CollectBehaviour colBeh = this.GetComponent<CollectBehaviour>();
        List<Item> itemListCopy = new List<Item>();
        for (int c = 0; c < colBeh.GetInventory().GetInventoryList().Count; c++)
        {
            itemListCopy.Add(colBeh.GetInventory().GetInventoryList()[c].ReturnDeepCopy());
        }
        SaveManager.instance.playerData.itemList = itemListCopy;
        ////////////////////
        ///SavePlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesSave = new List<PlayerNotes>();
        for(int n = 0;n<pData.pNotes.Count;n++)
        {
            pNotesSave.Add(pData.pNotes[n].ReturnDeepCopyNotes());
        }
        SaveManager.instance.playerData.playerNotes = pNotesSave;
        ///////////////////
        ///

        Debug.Log("PlayerSaved");
    }

    public void SavePlayerInventory()
    {
        CollectBehaviour colBeh = this.GetComponent<CollectBehaviour>();
        List<Item> itemListCopy = new List<Item>();
        for (int c = 0; c < colBeh.GetInventory().GetInventoryList().Count; c++)
        {
            itemListCopy.Add(colBeh.GetInventory().GetInventoryList()[c].ReturnDeepCopy());
        }
        SaveManager.instance.playerData.itemList = itemListCopy;
    }




    public bool CheckHasDataNPC()
    {
        for (int i = 0; i < SaveManager.instance.activeSave.stageData.Count; i++)
        {
            if (SceneManager.GetActiveScene().name == SaveManager.instance.activeSave.stageData[i].stageName)
            {
                for (int j = 0; j < SaveManager.instance.activeSave.stageData[i].dNPC.Count; j++)
                {
                    if (SaveManager.instance.activeSave.stageData[i].dNPC[j].nName == this.gameObject.name)
                    {
                        return true;
                    }
                }

            }
        }
        return false;
    }
    public void LoadNPC()
    {
        for (int i = 0; i < SaveManager.instance.activeSave.dNPC.Count; i++)
        {
            if (this.gameObject.name == SaveManager.instance.activeSave.dNPC[i].nName)
            {
                this.gameObject.transform.position = SaveManager.instance.activeSave.dNPC[i].npcPosition;
                this.GetComponent<RegularAI>().index = SaveManager.instance.activeSave.dNPC[i].dialIndex;
            }
        }
    }
    public void SaveNPC()
    {
        DataTypeNPC nPC = new DataTypeNPC();
        nPC.nName = this.gameObject.name;
        nPC.npcPosition = this.transform.position;
        RegularAI ai = this.GetComponent<RegularAI>();
        nPC.dialIndex = ai.index;
        SaveManager.instance.activeSave.dNPC.Add(nPC);
        Debug.Log("NPC Save");
    }

    public bool CheckHasDataObject()
    {
        for (int i = 0; i < SaveManager.instance.activeSave.stageData.Count; i++)
        {
            if (SceneManager.GetActiveScene().name == SaveManager.instance.activeSave.stageData[i].stageName)
            {
                for (int j = 0; j < SaveManager.instance.activeSave.stageData[i].dObject.Count; j++)
                {
                    if (SaveManager.instance.activeSave.stageData[i].dObject[j].oName == this.gameObject.name)
                    {
                        return true;
                    }
                }

            }
        }
        return false;
    }

    public void LoadObject()
    {
        for (int i = 0; i < SaveManager.instance.activeSave.stageData.Count; i++)
        {
            if (SceneManager.GetActiveScene().name == SaveManager.instance.activeSave.stageData[i].stageName)
            {
                for (int j = 0; j < SaveManager.instance.activeSave.stageData[i].dObject.Count; j++)
                {
                    if (SaveManager.instance.activeSave.stageData[i].dObject[j].oName == this.gameObject.name)
                    {
                        //SaveManager.instance.activeSave.stageData[i].dObject[j].hasBeenCollected;
                    }
                }

            }
        }
    }
    public void SaveObject()
    {
        DataTypeObject obj = new DataTypeObject();
        obj.oName = this.gameObject.name;
        obj.objectPosition = this.transform.position;
        for (int i = 0; i < SaveManager.instance.activeSave.stageData.Count; i++)
        {
            if (SceneManager.GetActiveScene().name == SaveManager.instance.activeSave.stageData[i].stageName)
            {
                SaveManager.instance.activeSave.stageData[i].dObject.Add(obj);
                break;
            }
        }
    }
}
