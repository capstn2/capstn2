﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapUI : MonoBehaviour
{
    public GameObject mapUI;
    private bool isOpen = false;
    void Start()
    {
        Referencer.Instance.miniMapUI = this;
        mapUI.SetActive(false);
    }


    public void OpenOrCloseMap()
    {
        if(isOpen)
        {
            mapUI.SetActive(false);
            isOpen = false;
        }
        else
        {
            mapUI.SetActive(true);
            isOpen = true;
        }
    }
}
