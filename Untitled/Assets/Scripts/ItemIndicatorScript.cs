﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemIndicatorScript : MonoBehaviour
{
    private bool isActive = false;
    public GameObject item;
    private GameObject visual;
    private GameObject player;
    public Sprite itemIndi;
    public Sprite grabIndi;

    public bool canGrabItem;
    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(item.transform.position.x, item.transform.position.y+3, item.transform.position.z);
        visual = this.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            Vector3 targetVector = Camera.main.transform.position - this.transform.position;

            float newAngle = Mathf.Atan2(targetVector.z, targetVector.x) * Mathf.Rad2Deg;

            this.transform.rotation = Quaternion.Euler(0, -1 * newAngle, 0);
            /*
            if (Vector3.Distance(item.transform.position, player.transform.position) < 6)
            {
                visual.GetComponent<SpriteRenderer>().sprite = grabIndi;
            }
            else
            {
                visual.GetComponent<SpriteRenderer>().sprite = itemIndi;
            }*/
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            isActive = true;
            player = other.gameObject;
            visual.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isActive = false;
            player = null;
            visual.SetActive(false);
        }
    }
}
