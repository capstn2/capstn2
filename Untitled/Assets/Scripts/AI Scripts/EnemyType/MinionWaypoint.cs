﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinionWaypoint : MonoBehaviour
{
    [SerializeField] private Transform startingDestination;
    [SerializeField] private List<Transform> waypoints = new List<Transform>();
    private List<Collider> alreadyPingedColliderList;

    private void Start()
    {

        Patrol.OnReachStartingLocation += Patrol_OnReachStartingLocation;;
    }

    void Patrol_OnReachStartingLocation(object sender, System.EventArgs e)
    {
        LocateWaypoints();
    }


    public List<Transform> GetWaypoints()
    {
        return waypoints;
    }

    public Transform GetStartingDestinaton()
    {
        return startingDestination;
    }

    private void LocateWaypoints()
    {
        RaycastHit[] hitArray = Physics.SphereCastAll(transform.position, 300f / 2f, Vector3.forward);
        foreach (RaycastHit hit in hitArray)
        {
            if (hit.collider != null)
            {
                //hit something
                if (!alreadyPingedColliderList.Contains(hit.collider))
                {
                    if (hit.collider.gameObject.tag == "Checkpoints")
                    {
                        var checkpoints = hit.collider.gameObject.tag == "Checkpoints";

                        waypoints.Add(hit.collider.transform);
                        alreadyPingedColliderList.Add(hit.collider);
                        Debug.Log("I hit something");
                    }
                }
            }
        }

    }



}
