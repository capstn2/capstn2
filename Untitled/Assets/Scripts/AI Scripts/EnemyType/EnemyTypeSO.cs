﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/EnemyType")]
public class EnemyTypeSO : ScriptableObject
{
    public string nameString;
    public EnemyType.EnemyTypes enemyTypes;
    public List<InteractableTypeSO> weakness;
    public GameObject prefab;
    public Animator animator;
    public int health;
    public int maxStunThreshold;


}
