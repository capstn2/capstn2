﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{


    [SerializeField] private EnemyTypeSO EnemyTypeSO;


    private bool isDead = false;
    private bool isBlind = false;
    private bool itemCollected = false;
    private float blindDuration = 3f;

    //Skill1 Setting//
    [SerializeField] private float skillCD = 20f;
    private float tempCD;
    private bool onCoolDown = false;
    private int usagePercentage = 10;

    //Special Skill Setting//
    [SerializeField] private float specialCD = 30f;
    private float specialTempCD;
    private bool onSpecialCoolDown = false;
    private int specialUsagePercentage = 100;

    //threshold--//
    [SerializeField] private int stunThreshold;
    private bool isStunned = false;
    private float stunDuration = 5f;

    //Aggro Setting//
    private bool isAggro = false;
    private float aggroDuration = 10f;
    private float tempAggroDuration;

    //Alert Setting//
    private bool isAlerted = false;
    [SerializeField] private Vector3 locationAlerted;
    private float alertDuration = 20f;
    private float tempAlertDuration;

    private bool isEnrage = false;


    private int provokeCounter = 0;

    public Enemy CreateEnemy(Vector3 position)
    {
        Transform pfEnemy = EnemyTypeSO.prefab.transform;
        Transform enemyTransform = Instantiate(pfEnemy, position, Quaternion.identity);

        Enemy enemy = enemyTransform.GetComponent<Enemy>();

        return enemy;
    }

    private void Awake()
    {
       
        stunThreshold = EnemyTypeSO.maxStunThreshold;
    }


    // Start is called before the first frame update
    public virtual void Start()
    {
        tempCD = skillCD;
        specialTempCD = specialCD;
        tempAggroDuration = aggroDuration;

        BlessAbilityBehaviour.OnBlindUnit += BlessAbilityBehaviour_OnBlindUnit;
        Gun.OnGunShot += Gun_OnGunShot;

        Die.OnDie += (object sender, EventArgs e) => { isDead = true;  Debug.Log(isDead); };
        PistolProjectile.OnEnemyHit += (object sender, EventArgs e) => { stunThreshold -= 10; if(!isAggro) isAggro = true; };
        ButtonMashControl.OnButtonMashFinished += ButtonMashControl_OnButtonMashFinished;
        Fireball.OnFireballHit += Fireball_OnFireballHit;
        Freeze.OnFreezeHit += Freeze_OnFreezeHit;
        LightEffect.OnLightHit += LightEffect_OnLightHit;
        Fireball.OnFireballShot += Fireball_OnFireballShot;
        Freeze.OnFreezeShot += Freeze_OnFreezeShot;
        CorpseObjective.OnObjectiveComplete += CorpseObjective_OnObjectiveComplete;

    }

    void CorpseObjective_OnObjectiveComplete(object sender, EventArgs e)
    {
        isEnrage = true;
    }


    void Fireball_OnFireballShot(object sender, EventArgs e)
    {
        isAlerted = true;
    }

    void Freeze_OnFreezeShot(object sender, EventArgs e)
    {
        isAlerted = true;
    }

    void Fireball_OnFireballHit(object sender, EventArgs e)
    {
        isStunned = true;
    }

    void Freeze_OnFreezeHit(object sender, EventArgs e)
    {
        isStunned = true;
    }

    void LightEffect_OnLightHit(object sender, EventArgs e)
    {
        isStunned = true;
    }

    void ButtonMashControl_OnButtonMashFinished(object sender, EventArgs e)
    {
        isStunned = true;
    }


    void Gun_OnGunShot(object sender, EventArgs e)
    {
        float dist = Vector3.Distance(this.GetComponent<EnemyAI>().player.position, this.transform.position);
        Debug.Log(dist);

        if(dist < 100)
        {
            isAlerted = true;
            locationAlerted = GetComponent<EnemyAI>().player.position;
            Debug.Log("isAlerted");
        }
        Debug.Log("Not close enough to be alerted");


    }


    void BlessAbilityBehaviour_OnBlindUnit(object sender, EventArgs e)
    {
        isBlind = true;
        Debug.Log(isBlind);
    }


    // Update is called once per frame
    void Update()
    {
        if(stunThreshold <= 0)
        {
            SetIsStunned(true);

            Debug.Log("Enemy is Stunned");
        }

        if (isStunned)
        {
            Debug.Log("Stun Duration: " + stunDuration);
            stunDuration -= Time.deltaTime;
            if (stunDuration <= 0f)
            {
                SetIsStunned(false);
                           
                Debug.Log("SetIsStunned: " + isStunned);
                stunDuration = 5f;
                stunThreshold = EnemyTypeSO.maxStunThreshold;

            }
        }

        if (isAggro)
        {
            Debug.Log("Aggro-ed the Enemy");
            aggroDuration -= Time.deltaTime;
            if(aggroDuration <= 0f)
            {
                SetAggro(false);
                aggroDuration = tempAggroDuration;
            }
        }

        if (isAlerted)
        {
            Debug.Log("Alerted by something");
            alertDuration -= Time.deltaTime;
            if(alertDuration <= 0f)
            {
                SetAlerted(false);
                alertDuration = tempAlertDuration;
            }
        }


        if (GetCoolDown())
        {
            SkillCoolDown();
        }

        if (GetSpecialCD())
        {
            SpecialSkillCoolDown();
        }

        if (isBlind)
        {
            Debug.Log(blindDuration);
            blindDuration -= Time.deltaTime;
            if (blindDuration <= 0f)
            {
                SetBlind(false);
                Debug.Log("SetBlind: " + isBlind);
                blindDuration = 3f;
            }

        }

        //if (isBlind)
        //{
        //    return;
        //}

        //Vector3 moveDir = (targetTransform.position - transform.position).normalized;

        //float moveSpeed = 6f;
        //rigidbody.velocity = moveDir * moveSpeed;
    }

    private void SkillCoolDown()
    {

        skillCD -= Time.deltaTime;
        if (skillCD <= 0)
        {
            SetCoolDown(false);
            skillCD = tempCD;

        }
        
    }

    private void SpecialSkillCoolDown()
    {
        specialCD -= Time.deltaTime;
        if(specialCD <= 0)
        {
            SetSpecialCD(false);
            specialCD = specialTempCD;
        }
    }

    public virtual void Attack()
    {
        Debug.Log("I am Attacking");
    }

  


    private void RandomLocation()
    {

        float randX = UnityEngine.Random.Range(-5, 5);
        float randZ = UnityEngine.Random.Range(-5, 5);
        Vector3 newLocation = new Vector3(randX, transform.position.y, randZ);


        Vector3 moveDir = (newLocation - transform.position).normalized;

    }

    #region Properties
    public void SetBlind(bool blind)
    {
        isBlind = blind;
    }

    public bool GetBlind()
    {
        return isBlind;
    }

    public EnemyTypeSO GetEnemyTypeSO()
    {
        return EnemyTypeSO;
    }

    public bool GetCollectAllItem()
    {
        return itemCollected;
    }

    public void SetCollectAllItem(bool value)
    {
        itemCollected = value;
    }

    public void SetCoolDown(bool cd)
    {
        onCoolDown = cd;
    }

    public bool GetCoolDown()
    {
        return onCoolDown;
    }

    public int GetUsagePercentage()
    {
        return usagePercentage;
    }

    public bool GetIsStunned()
    {
        return isStunned;
    }

    public void SetIsStunned(bool value)
    {
        isStunned = value;
    }

    public bool GetAggro()
    {
        return isAggro;
    }

    public void SetAggro(bool value)
    {
        isAggro = value;
    }

    public int GetProvokeCounter()
    {
        return provokeCounter;
    }

    public void SetProvokeCounter(int count)
    {
        provokeCounter = count;
    }

    public bool GetAlerted()
    {
        return isAlerted;
    }

    public void SetAlerted(bool value)
    {
        isAlerted = value;
    }

    public Vector3 GetLocationAlerted()
    {
        return locationAlerted;
    }

    public bool GetSpecialCD()
    {
        return onSpecialCoolDown;
    }

    public void SetSpecialCD(bool value)
    {
        onSpecialCoolDown = value;
    }

    public int GetSpecialUsagePercentage()
    {
        return specialUsagePercentage;
    }

    public bool GetEnrage()
    {
        return isEnrage;
    }



    #endregion



}

[Serializable]
public class EnemyType
{
    public enum EnemyTypes
    {
        Manananggal,
        Tiyanak,
        Mangkukulam,
        LesserFlameWisps,
        Aswang,
        Aswang2
    };
}
