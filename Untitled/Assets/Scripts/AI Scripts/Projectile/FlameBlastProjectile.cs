﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameBlastProjectile : MonoBehaviour
{
    //Mangkukulam Flame blast 
    private float projectileSpeed = 3000f;
    private Transform unitTransform;
    private Transform firePoint;


    private void Awake()
    {
        unitTransform = FindObjectOfType<EnemyRangeAttribute>().transform;
        firePoint = unitTransform.transform.Find("firePoint");
    }

    // Start is called before the first frame update
    void Start()
    {
        transform.GetComponent<Rigidbody>().velocity = firePoint.transform.forward * (projectileSpeed * Time.deltaTime);

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(firePoint.position, transform.position) > unitTransform.GetComponent<EnemyRangeAttribute>().GetRange())
        {
            DestroyFireball();
        }
    }


    public void DestroyFireball()
    {
        Destroy(gameObject);
    }
}
