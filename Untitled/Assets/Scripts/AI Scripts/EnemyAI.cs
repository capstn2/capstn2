﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    private NavMeshAgent agent;
    [SerializeField] private Animator anim;
    [SerializeField] private AudioSource audioSource;
    public Transform player;
    [SerializeField] private State currentState;





    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        currentState = new Idle(this.gameObject, agent, anim, player,audioSource);

    }

    // Update is called once per frame
    void Update()
    {
        currentState = currentState.Process();
    }



   
}
