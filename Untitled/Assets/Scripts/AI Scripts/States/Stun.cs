﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Stun : State
{
    static public EventHandler OnStunFinish;

    public Stun(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.STUN;
        agent.speed = 0;
    }

    public override void Enter()
    {
        //Stun Anim
        anim.SetTrigger("IsStun");
        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                if (!npc.GetComponent<Enemy>().GetIsStunned())
                {
                    OnStunFinish?.Invoke(this, EventArgs.Empty);
                    nextState = new Patrol(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;


            case EnemyType.EnemyTypes.Mangkukulam:
                if (!npc.GetComponent<Enemy>().GetIsStunned())
                {
                    if(npc.GetComponent<Enemy>().GetProvokeCounter() == 3)
                    {
                        npc.GetComponent<Enemy>().SetProvokeCounter(0);
                        nextState = new SpecialSkill(npc, agent, anim, player,audioSource);
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                    nextState = new Patrol(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;

            case EnemyType.EnemyTypes.Aswang:
                {
                   
                    if (!npc.GetComponent<Enemy>().GetIsStunned())
                    {
                        OnStunFinish?.Invoke(this, EventArgs.Empty);
                        nextState = new Patrol(npc, agent, anim, player,audioSource);
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                    break;
                }

            case EnemyType.EnemyTypes.Aswang2:
                {

                    if (!npc.GetComponent<Enemy>().GetIsStunned())
                    {
                        OnStunFinish?.Invoke(this, EventArgs.Empty);
                        nextState = new Patrol(npc, agent, anim, player,audioSource);
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                    break;
                }
        }
    }

    public override void Exit()
    {
        //Reset Anim
        anim.ResetTrigger("IsStun");
        base.Exit();
    }
}
