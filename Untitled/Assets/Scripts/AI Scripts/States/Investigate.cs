﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Investigate : State
{
   
    public Investigate(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.INVESTIGATE;
        agent.speed = 4f;
    }

    public override void Enter()
    {
        Debug.Log("Investigating Area");
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:

                Vector3 location = npc.GetComponent<Enemy>().GetLocationAlerted();
                anim.SetTrigger("IsChasing1");
                //Set Anim
                agent.SetDestination(location);
                break;

            case EnemyType.EnemyTypes.Mangkukulam:

                location = npc.GetComponent<Enemy>().GetLocationAlerted();
                //Set Anim
                agent.SetDestination(location);
                break;



            case EnemyType.EnemyTypes.Aswang:
                var prefab = npc.transform.Find("Aswang");
                float dist = Vector3.Distance(player.position, npc.transform.position);

                if(dist < 60)
                {
                    AswangObsEvent.Instance.OnPlayerSawAswangInvisible();
                }

               

                prefab.gameObject.SetActive(false);

                location = npc.GetComponent<Enemy>().GetLocationAlerted();
                //Set Anim
                agent.SetDestination(location);
                break;

            case EnemyType.EnemyTypes.Aswang2:
                prefab = npc.transform.Find("Aswang");

                float dist2 = Vector3.Distance(player.position, npc.transform.position);

                if (dist2 < 60)
                {
                    AswangObsEvent.Instance.OnPlayerSawAswangInvisible();
                }

                prefab.gameObject.SetActive(false);


                location = npc.GetComponent<Enemy>().GetLocationAlerted();
                //Set Anim
                agent.SetDestination(location);
                break;
        }

       
        base.Enter();
    }

    public override void Update()
    {
        if (CanSeePlayer())
        {
            nextState = new Chase(npc, agent, anim, player,audioSource);
            stage = EVENT.EXIT;
        }

        if (agent.remainingDistance < 1)
        {
            nextState = new Wander(npc, agent, anim, player,audioSource);
            stage = EVENT.EXIT;
        }


    }

    public override void Exit()
    {
        //Reset anim
        anim.ResetTrigger("IsChasing1");
        base.Exit();
    }
}
