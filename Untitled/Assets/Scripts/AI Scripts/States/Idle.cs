﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Idle : State
{

    public Idle(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.IDLE;
    }

    public override void Enter()
    {
        //set anim;
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {

            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                //Set Manananggal Anim//
                anim.SetTrigger("IsIdle");
                Debug.Log("isIdle");
                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                //Set Mangkukulam Anim//
                Debug.Log("Mangkukulam isIdle");
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.SetTrigger("IsIdle");
                Debug.Log("Aswang isIdle");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.SetTrigger("IsIdle");
                Debug.Log("Aswang isIdle");
                break;
        }
        base.Update();
    }


    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                if (npc.GetComponent<Enemy>().GetCollectAllItem())
                {
                    nextState = new Return(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                else
                {
                    if (CanSeePlayer())
                    {

                        nextState = new Chase(npc, agent, anim, player, audioSource);

                        stage = EVENT.EXIT;
                    }
                    else if (Random.Range(0, 100) < 10)
                    {
                        nextState = new Patrol(npc, agent, anim, player,audioSource);
                        stage = EVENT.EXIT;
                    }

                }
                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                else
                {
                    if (CanSeePlayer())
                    {
                        nextState = new Chase(npc, agent, anim, player ,audioSource);

                        stage = EVENT.EXIT;
                    }
                    else if (Random.Range(0, 100) < 10)
                    {
                        nextState = new Patrol(npc, agent, anim, player, audioSource);
                        stage = EVENT.EXIT;
                    }
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.LesserFlameWisps:
                #region Lesser Flame Wisp
                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player ,audioSource);

                    stage = EVENT.EXIT;
                }
                else if (Random.Range(0, 100) < 10)
                {
                    nextState = new Wander(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                break;
            #endregion


            case EnemyType.EnemyTypes.Aswang:
                #region

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                else
                {
                    if (CanSeePlayer())
                    {
                        nextState = new Chase(npc, agent, anim, player, audioSource);

                        stage = EVENT.EXIT;
                    }
                    else if (Random.Range(0, 100) < 10)
                    {
                        nextState = new Return(npc, agent, anim, player, audioSource);
                        stage = EVENT.EXIT;
                    }
                }

                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang2:
                #region

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                else
                {
                    if (CanSeePlayer())
                    {
                        nextState = new Chase(npc, agent, anim, player,audioSource);

                        stage = EVENT.EXIT;
                    }
                    else if (Random.Range(0, 100) < 10)
                    {
                        nextState = new Return(npc, agent, anim, player, audioSource);
                        stage = EVENT.EXIT;
                    }
                }

                break;
                #endregion

        }




    }


    public override void Exit()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {

            case EnemyType.EnemyTypes.Manananggal:
                //reset anim
                anim.ResetTrigger("IsIdle");
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.ResetTrigger("IsIdle");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.ResetTrigger("IsIdle");
                break;
        }
               
                base.Exit();
    }

}
