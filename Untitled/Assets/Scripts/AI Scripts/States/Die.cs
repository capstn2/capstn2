﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Die : State
{
    static public EventHandler OnDie;

    public Die(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.DIE;
    }

    public override void Enter()
    {
        Debug.Log("isDead");
        audioSource.clip = npc.GetComponent<ManananggalSounds>().deadSound;
        audioSource.loop = false;
        audioSource.Play();

        npc.gameObject.SetActive(false);
        OnDie?.Invoke(this, EventArgs.Empty);
        base.Enter();
    }


    public override void Update()
    {
       
    }


    public override void Exit()
    {
        base.Exit();
    }
}
