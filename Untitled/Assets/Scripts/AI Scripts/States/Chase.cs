﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chase : State
{
    static public EventHandler OnChasePlayer;
    static public EventHandler OnEndChase;


    public Chase(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.CHASE;
        agent.speed = 10;
        agent.isStopped = false;
    }

    public override void Enter()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {

            case EnemyType.EnemyTypes.Manananggal:
                //Set Manananggal Anim//
                AswangObsEvent.Instance.OnPlayerSeeManananggalNoLowerBody();
                OnChasePlayer?.Invoke(this, EventArgs.Empty);
                anim.SetTrigger("IsChasing1");
                Debug.Log("isChasing");
                break;

            case EnemyType.EnemyTypes.Mangkukulam:
                //Set Mangkukulam Anim//
                Debug.Log("Mangkukulam isChasing");
                break;

            case EnemyType.EnemyTypes.Aswang:
                Debug.Log("Aswang is Chasing");
                var prefab = npc.transform.Find("Aswang");
                OnChasePlayer?.Invoke(this, EventArgs.Empty);
                if (!prefab.gameObject.activeInHierarchy)
                {
                    prefab.gameObject.SetActive(true);
                }
                anim.SetTrigger("IsRunning");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                Debug.Log("Aswang is Chasing");
                prefab = npc.transform.Find("Aswang");
                OnChasePlayer?.Invoke(this, EventArgs.Empty);
                if (!prefab.gameObject.activeInHierarchy)
                {
                    prefab.gameObject.SetActive(true);
                }
                anim.SetTrigger("IsRunning");
                break;
        }
        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {

            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                if (npc.GetComponent<Enemy>().GetCollectAllItem())
                {
                    nextState = new Die(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                else
                {
                    agent.SetDestination(player.position);
                    if (agent.hasPath)
                    {
                        if (npc.GetComponent<Enemy>().GetCollectAllItem())
                        {
                            nextState = new Return(npc, agent, anim, player,audioSource);
                            stage = EVENT.EXIT;
                        }

                        if (npc.GetComponent<Enemy>().GetBlind())
                        {
                            nextState = new Fear(npc, agent, anim, player,audioSource);
                            stage = EVENT.EXIT;
                        }

                        if (CanAttackPlayer())
                        {
                            int specialPercentage = npc.GetComponent<Enemy>().GetSpecialUsagePercentage();
                            bool onSpecialCoolDown = npc.GetComponent<Enemy>().GetSpecialCD();

                            if (!onSpecialCoolDown)
                            {
                                if (UnityEngine.Random.Range(0f, 1000f) < specialPercentage)
                                {
                                    nextState = new SpecialSkill(npc, agent, anim, player,audioSource);
                                    previousState = this;
                                    stage = EVENT.EXIT;
                                }
                            }
                            else
                            {
                                nextState = new Attack(npc, agent, anim, player,audioSource); //TODO Do Attack State
                                previousState = this;
                                stage = EVENT.EXIT;
                            }

                           
                        }
                        else if (!CanAttackPlayer())
                        {
                            int skillPercentage = npc.GetComponent<Enemy>().GetUsagePercentage();
                            bool onCooldown = npc.GetComponent<Enemy>().GetCoolDown();
                            if (!onCooldown)
                            {
                                if (UnityEngine.Random.Range(0f, 1000f) < skillPercentage)
                                {
                                    nextState = new Provoke(npc, agent, anim, player,audioSource);
                                    previousState = this;
                                    stage = EVENT.EXIT;
                                }
                            }
                            else
                            {
                                nextState = new Chase(npc, agent, anim, player,audioSource);
                                previousState = this;
                                stage = EVENT.EXIT;
                            }

                        }
                        if (!CanSeePlayer())
                        {
                            if (npc.GetComponent<Enemy>().GetAggro())
                            {
                                agent.SetDestination(player.position);
                            }
                            else
                            {
                                OnEndChase?.Invoke(this, EventArgs.Empty);
                                nextState = new Wander(npc, agent, anim, player,audioSource);
                                previousState = this;
                                stage = EVENT.EXIT;
                            }
                           
                        }
                    }
                }

                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                else
                {
                    agent.SetDestination(player.position);
                    if (agent.hasPath)
                    {
                        if (npc.GetComponent<Enemy>().GetBlind())
                        {
                            nextState = new Fear(npc, agent, anim, player,audioSource);
                            stage = EVENT.EXIT;
                        }

                        if (CanAttackPlayer())
                        {
                    
                            nextState = new Attack(npc, agent, anim, player,audioSource); //TODO Do Attack State
                            previousState = this;
                            stage = EVENT.EXIT;

                        }
                        else if (!CanAttackPlayer())
                        {
                            var distance = Vector3.Distance(npc.transform.position, player.transform.position);
                            if(distance > 15f)
                            {
                                int skillPercentage = npc.GetComponent<Enemy>().GetUsagePercentage();
                                bool onCooldown = npc.GetComponent<Enemy>().GetCoolDown();

                               
                                if (!onCooldown)
                                {
                                    if (UnityEngine.Random.Range(0f, 1000f) < skillPercentage)
                                    {
                                        nextState = new SecondaryAttack(npc, agent, anim, player,audioSource);
                                        previousState = this;
                                        stage = EVENT.EXIT;
                                    }
                                }
                                
                            }
                            else
                            {
                                nextState = new Chase(npc, agent, anim, player,audioSource);
                                previousState = this;
                                stage = EVENT.EXIT;
                            }

                        }
                        if (!CanSeePlayer())
                        {
                            nextState = new Wander(npc, agent, anim, player,audioSource);
                            previousState = this;
                            stage = EVENT.EXIT;
                        }
                    }

                }
                break;
            #endregion

            case EnemyType.EnemyTypes.LesserFlameWisps: // Mangkukulam Minion
                #region Lesser Flame Wisps
                agent.SetDestination(player.position);
                if (agent.hasPath)
                {

                    if (CanAttackPlayer())
                    {
                        nextState = new Attack(npc, agent, anim, player,audioSource); //TODO Do Attack State
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                    else
                    {
                        nextState = new Chase(npc, agent, anim, player,audioSource);
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang:
                #region

                agent.SetDestination(player.position);
                if (agent.hasPath)
                {
                    if (npc.GetComponent<Enemy>().GetEnrage())
                    {
                        nextState = new Provoke(npc, agent, anim, player, audioSource);
                        stage = EVENT.EXIT;
                    }
                    if (npc.GetComponent<Enemy>().GetBlind())
                    {
                        nextState = new Fear(npc, agent, anim, player,audioSource);
                        stage = EVENT.EXIT;
                    }

                    if (npc.GetComponent<Enemy>().GetIsStunned())
                    {
                        nextState = new Stun(npc, agent, anim, player,audioSource);
                        stage = EVENT.EXIT;
                    }

                    if (CanAttackPlayer())
                    {
                        int specialPercentage = npc.GetComponent<Enemy>().GetSpecialUsagePercentage();
                        bool onSpecialCoolDown = npc.GetComponent<Enemy>().GetSpecialCD();

                        if (!onSpecialCoolDown)
                        {
                            if (UnityEngine.Random.Range(0f, 1000f) < specialPercentage)
                            {
                                nextState = new SpecialSkill(npc, agent, anim, player,audioSource);
                                previousState = this;
                                stage = EVENT.EXIT;
                            }
                        }
                        else
                        {
                            nextState = new Attack(npc, agent, anim, player,audioSource); //TODO Do Attack State
                            previousState = this;
                            stage = EVENT.EXIT;
                        }
                    }
                    else if (!CanAttackPlayer())
                    {
                        var distance = Vector3.Distance(npc.transform.position, player.transform.position);
                        if (distance > 15f)
                        {
                            int skillPercentage = npc.GetComponent<Enemy>().GetUsagePercentage();
                            bool onCooldown = npc.GetComponent<Enemy>().GetCoolDown();
                            if (!onCooldown)
                            {
                                if (UnityEngine.Random.Range(0f, 1000f) < skillPercentage)
                                {
                                    nextState = new Provoke(npc, agent, anim, player,audioSource);
                                    previousState = this;
                                    stage = EVENT.EXIT;
                                }
                            }
                        }
                        else
                        {
                            nextState = new Chase(npc, agent, anim, player,audioSource);
                            previousState = this;
                            stage = EVENT.EXIT;
                        }

                    }
                    if (!CanSeePlayer())
                    {
                        OnEndChase?.Invoke(this, EventArgs.Empty);
                        nextState = new Wander(npc, agent, anim, player,audioSource);
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                }

                    break;
            #endregion
            case EnemyType.EnemyTypes.Aswang2:
                #region
                agent.SetDestination(player.position);
                if (agent.hasPath)
                {
                    if (npc.GetComponent<Enemy>().GetEnrage())
                    {
                        nextState = new Provoke(npc, agent, anim, player, audioSource);
                        stage = EVENT.EXIT;
                    }

                    if (npc.GetComponent<Enemy>().GetBlind())
                    {
                        nextState = new Fear(npc, agent, anim, player,audioSource);
                        stage = EVENT.EXIT;
                    }

                    if (npc.GetComponent<Enemy>().GetIsStunned())
                    {
                        nextState = new Stun(npc, agent, anim, player,audioSource);
                        stage = EVENT.EXIT;
                    }

                    if (CanAttackPlayer())
                    {
                        int specialPercentage = npc.GetComponent<Enemy>().GetSpecialUsagePercentage();
                        bool onSpecialCoolDown = npc.GetComponent<Enemy>().GetSpecialCD();

                        if (!onSpecialCoolDown)
                        {
                            if (UnityEngine.Random.Range(0f, 1000f) < specialPercentage)
                            {
                                nextState = new SpecialSkill(npc, agent, anim, player,audioSource);
                                previousState = this;
                                stage = EVENT.EXIT;
                            }
                        }
                        else
                        {
                            nextState = new Attack(npc, agent, anim, player,audioSource); //TODO Do Attack State
                            previousState = this;
                            stage = EVENT.EXIT;
                        }
                    }
                    else if (!CanAttackPlayer())
                    {
                        var distance = Vector3.Distance(npc.transform.position, player.transform.position);
                        if (distance > 15f)
                        {
                            int skillPercentage = npc.GetComponent<Enemy>().GetUsagePercentage();
                            bool onCooldown = npc.GetComponent<Enemy>().GetCoolDown();
                            if (!onCooldown)
                            {
                                if (UnityEngine.Random.Range(0f, 1000f) < skillPercentage)
                                {
                                    nextState = new Provoke(npc, agent, anim, player,audioSource);
                                    previousState = this;
                                    stage = EVENT.EXIT;
                                }
                            }
                        }
                        else
                        {
                            nextState = new Chase(npc, agent, anim, player,audioSource);
                            previousState = this;
                            stage = EVENT.EXIT;
                        }

                    }
                    if (!CanSeePlayer())
                    {
                        OnEndChase?.Invoke(this, EventArgs.Empty);
                        nextState = new Wander(npc, agent, anim, player,audioSource);
                        previousState = this;
                        stage = EVENT.EXIT;
                    }
                }

                break;
                #endregion
        }




    }

    public override void Exit()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                //Reset anim//
                anim.ResetTrigger("IsChasing1");
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.ResetTrigger("IsRunning");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.ResetTrigger("IsRunning");
                break;
        }

        base.Exit();
    }


}
