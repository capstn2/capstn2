﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Attack : State
{
    static public EventHandler OnMangkukulamAttack;


    private float rotationSpeed = 2.0f;
    private float attackRate = 1f;

   
    public Attack(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.ATTACK;
    }

    public override void Enter()
    {
        Debug.Log("isAttacking");
        BasicAttack();
        agent.isStopped = true;


        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {

            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                attackRate -= Time.deltaTime;
                if (npc.GetComponent<Enemy>().GetCollectAllItem())
                {
                    nextState = new Return(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                Vector3 direction = player.position - npc.transform.position;
                float angle = Vector3.Angle(direction, npc.transform.forward);
                direction.y = 0;

                npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);


                if (CanAttackPlayer() && attackRate <= 0)
                {
                    nextState = new Attack(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                if (!CanAttackPlayer())
                {
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
                #endregion

            case EnemyType.EnemyTypes.Mangkukulam:

                #region Mangkukulam
                attackRate -= Time.deltaTime;
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                direction = player.position - npc.transform.position;
                angle = Vector3.Angle(direction, npc.transform.forward);
                direction.y = 0;

                npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);


                if (CanAttackPlayer() && attackRate <= 0)
                {
                    nextState = new Attack(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                if (!CanAttackPlayer())
                {
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
                #endregion

            case EnemyType.EnemyTypes.LesserFlameWisps:
                #region Lesser Flame Wisp
                nextState = new Die(npc, agent, anim, player,audioSource);
                previousState = this;
                stage = EVENT.EXIT;
                break;
                #endregion

            case EnemyType.EnemyTypes.Aswang:
                #region Aswang
                attackRate -= Time.deltaTime;
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                direction = player.position - npc.transform.position;
                angle = Vector3.Angle(direction, npc.transform.forward);
                direction.y = 0;

                npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);


                if (CanAttackPlayer() && attackRate <= 0)
                {
                    nextState = new Attack(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                if (!CanAttackPlayer())
                {
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;

            #endregion

            case EnemyType.EnemyTypes.Aswang2:
                #region Aswang
                attackRate -= Time.deltaTime;
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                direction = player.position - npc.transform.position;
                angle = Vector3.Angle(direction, npc.transform.forward);
                direction.y = 0;

                npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);


                if (CanAttackPlayer() && attackRate <= 0)
                {
                    nextState = new Attack(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                if (!CanAttackPlayer())
                {
                    nextState = new Idle(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;

                #endregion
        }

    }


    public override void Exit()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                //Reset anim
                anim.ResetTrigger("IsAttacking");
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.ResetTrigger("IsAttacking");
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.ResetTrigger("IsAttacking");
                break;
        }
       
        base.Exit();
    }


    private void BasicAttack()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                Debug.Log("Claw Attack");
                //Claw Anim
                anim.SetTrigger("IsAttacking");
                player.GetComponent<HealthScript>().DamageUnit(10);
                break;

            case EnemyType.EnemyTypes.Tiyanak:
                Debug.Log("Tiyanak Attacking");
                player.GetComponent<HealthScript>().DamageUnit(10);
                break;

            case EnemyType.EnemyTypes.Mangkukulam:
                Debug.Log("Mangkukulam Attacking");
                player.GetComponent<HealthScript>().DamageUnit(20);
                OnMangkukulamAttack?.Invoke(this, EventArgs.Empty);
                break;

            case EnemyType.EnemyTypes.LesserFlameWisps:
                Debug.Log("Explode");
                player.GetComponent<HealthScript>().DamageUnit(10);

                break;

            case EnemyType.EnemyTypes.Aswang:

                Debug.Log("Aswang Claw");
                player.GetComponent<HealthScript>().DamageUnit(10);
                anim.SetTrigger("IsAttacking");


                break;

            case EnemyType.EnemyTypes.Aswang2:

                Debug.Log("Aswang Claw");
                player.GetComponent<HealthScript>().DamageUnit(10);
                anim.SetTrigger("IsAttacking");


                break;
        }
    }

  

    private void SpecialAttack()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                Debug.Log("I am Biting");
                break;
            case EnemyType.EnemyTypes.Tiyanak:
                Debug.Log("Tiyanak Special");
                break;
        }
    }



}
