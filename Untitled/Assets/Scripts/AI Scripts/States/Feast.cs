﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Feast : State
{
    int currentIndex = -1;
    int currentIndex2 = -1;
    float feastDuration = 10f;
    float feastDuration2 = 10f;
    float resetDuration2;
    float resetDuration;

    public Feast(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.FEAST;
        agent.isStopped = false;
    }

    public override void Enter()
    {
        resetDuration = feastDuration;
        resetDuration2 = feastDuration2;
        float lastDist = Mathf.Infinity;

        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Aswang:
                for (int i = 0; i < CorpseLocation.Singleton.Checkpoints.Count; i++)
                {
                    GameObject thisWP = CorpseLocation.Singleton.Checkpoints[i];
                    float distance = Vector3.Distance(npc.transform.position, thisWP.transform.position);
                    if (distance < lastDist)
                    {
                        currentIndex = i - 1;
                        lastDist = distance;
                    }
                }
                agent.SetDestination(CorpseLocation.Singleton.Checkpoints[currentIndex].transform.position);
                break;

            case EnemyType.EnemyTypes.Aswang2:
                for (int i = 0; i < CorpseLocation2.Singleton2.Checkpoints2.Count; i++)
                {
                    GameObject thisWP = CorpseLocation2.Singleton2.Checkpoints2[i];
                    float distance = Vector3.Distance(npc.transform.position, thisWP.transform.position);
                    if (distance < lastDist)
                    {
                        currentIndex2 = i - 1;
                        lastDist = distance;
                    }
                }
                agent.SetDestination(CorpseLocation2.Singleton2.Checkpoints2[currentIndex2+1].transform.position);
                break;

        }


        base.Enter();
    }

    public override void Update()
    {

        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Aswang:
                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }


                if (agent.remainingDistance < 1)
                {
                    float dist = Vector3.Distance(player.position, npc.transform.position);
                    Debug.Log(dist);

                    if (dist < 30)
                    {
                        AswangObsEvent.Instance.OnPlayerSeeAswangEatingCorpses();
                    }

                    feastDuration -= Time.deltaTime;
                    if (feastDuration <= 0)
                    {
                        if (currentIndex >= CorpseLocation.Singleton.Checkpoints.Count - 1)
                        {
                            currentIndex = 0;
                        }
                        else
                        {

                            currentIndex++;
                        }

                        if (CorpseLocation.Singleton.Checkpoints[currentIndex] == null)
                        {
                            currentIndex++;
                        }
                        else
                        {
                            agent.SetDestination(CorpseLocation.Singleton.Checkpoints[currentIndex].transform.position);
                            feastDuration = resetDuration;
                        }

                    }

                }
                if (npc.GetComponent<Enemy>().GetAlerted())
                {
                    nextState = new Investigate(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;

            case EnemyType.EnemyTypes.Aswang2:

                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }


                if (agent.remainingDistance < 1)
                {
                    float dist2 = Vector3.Distance(player.position, npc.transform.position);
                    Debug.Log(dist2);

                    if (dist2 < 30)
                    {
                        AswangObsEvent.Instance.OnPlayerSeeAswangEatingCorpses();
                    }


                    feastDuration2 -= Time.deltaTime;
                    if (feastDuration2 <= 0)
                    {
                        if (currentIndex2 >= CorpseLocation2.Singleton2.Checkpoints2.Count - 1)
                        {
                            currentIndex2 = 0;
                        }
                        else
                        {

                            currentIndex2++;
                        }

                        if (CorpseLocation2.Singleton2.Checkpoints2[currentIndex2] == null)
                        {
                            currentIndex2++;
                        }
                        else
                        {
                            agent.SetDestination(CorpseLocation2.Singleton2.Checkpoints2[currentIndex2].transform.position);
                            feastDuration2 = resetDuration2;
                        }

                    }

                }
                if (npc.GetComponent<Enemy>().GetAlerted())
                {
                    nextState = new Investigate(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }


                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;



        }

       
    }

    public override void Exit()
    {
        base.Exit();
    }
}
