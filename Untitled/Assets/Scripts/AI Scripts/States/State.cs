﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

[System.Serializable]
public class State 
{
    static public EventHandler OnCollectAll;


    public enum STATE
    {
        IDLE, PATROL, CHASE, ATTACK, SECONDARYATTACK, SPECIALSKILL, WANDER, RETURN, FEAR, DIE, STUN, INVESTIGATE, PROVOKE,

        //---Mangkukulam Unique Skill---//
        CASTING,

        //--Aswang Unique Skill--//
        FEAST
    };

    public enum EVENT
    {
        ENTER, UPDATE, EXIT
    };

    public STATE name;
    protected EVENT stage;
    protected GameObject npc;
    protected Transform player;
    protected State nextState;
    protected State previousState = null;
    protected NavMeshAgent agent;
    protected Animator anim;
    protected AudioSource audioSource;

    protected float visDist = 30.0f;
    protected float visAngle = 70.0f;
    protected float attackDist = 5.0f;
    

    public State(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource)
    {
        npc = _npc;
        agent = _agent;
        stage = EVENT.ENTER;
        anim = _anim;
        audioSource = _audioSource;
        player = _player;

    }

    public virtual void Enter() { stage = EVENT.UPDATE; }
    public virtual void Update() { stage = EVENT.UPDATE; }
    public virtual void Exit() { stage = EVENT.EXIT; }

    public State Process()
    {
        if (stage == EVENT.ENTER) Enter();

        if (stage == EVENT.UPDATE) Update();
       
        if(stage == EVENT.EXIT)
        {
            Exit();
            return nextState;
        }
        return this;
    }


    public bool CanSeePlayer()
    {
        Vector3 direction = player.position - npc.transform.position;
        float angle = Vector3.Angle(direction, npc.transform.forward);

        if(direction.magnitude < visDist && angle < visAngle)
        {
            return true;
        }
        return false;
    }

    public bool CanAttackPlayer()
    {
        Vector3 direction = player.position - npc.transform.position;
        if(direction.magnitude < attackDist)
        {
            return true;
        }
        return false;
    }



}
