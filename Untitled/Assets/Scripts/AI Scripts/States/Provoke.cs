﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Provoke : State
{

    float frameCount = 2f;

    public Provoke(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        agent.isStopped = true;
    }

    public override void Enter()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                audioSource.clip = npc.GetComponent<ManananggalSounds>().provokeSound;
                audioSource.PlayOneShot(npc.GetComponent<ManananggalSounds>().provokeSound);
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.SetTrigger("IsProvoke");
                break;
            case EnemyType.EnemyTypes.Aswang2:
                anim.SetTrigger("IsProvoke");
                break;
        }

       
        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                frameCount -= Time.deltaTime;
                if (frameCount <= 0)
                {
                    nextState = new SecondaryAttack(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                break;


            case EnemyType.EnemyTypes.Aswang:
                frameCount -= Time.deltaTime;
                if (frameCount <= 0)
                {
                    nextState = new SecondaryAttack(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                break;

            case EnemyType.EnemyTypes.Aswang2:
                frameCount -= Time.deltaTime;
                if (frameCount <= 0)
                {
                    nextState = new SecondaryAttack(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }
                break;
        }
    }


    public override void Exit()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.Manananggal:
                agent.isStopped = false;
                base.Exit();
                break;

            case EnemyType.EnemyTypes.Aswang:
                anim.ResetTrigger("IsProvoke");
                agent.isStopped = false;
                base.Exit();
                break;

            case EnemyType.EnemyTypes.Aswang2:
                anim.ResetTrigger("IsProvoke");
                agent.isStopped = false;
                base.Exit();
                break;
        }



               
    }


   
}
