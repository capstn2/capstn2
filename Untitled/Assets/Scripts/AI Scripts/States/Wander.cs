﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Wander : State
{
    Vector3 wanderTarget = Vector3.zero;
    Transform previousTransform;
    float wanderRadius = 10; // 10
    float wanderDist = 20;  //20
    float wanderJitter = 10; //10
    float wanderTimer;
    float wanderTimerMax = 5f;

    private void Start()
    {
        wanderTimer = Random.Range(0f, wanderTimerMax);

        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {
            case EnemyType.EnemyTypes.LesserFlameWisps:
                wanderRadius = 40;
                var rand = Random.Range(0, 2); // Lesser Wisp has a chance to rotate to wander the opposite side
                if(rand == 0)
                {
                    wanderDist = 50;
                }
                else
                {
                    wanderDist = -50;
                }
                break;
        }
    }


    public Wander(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, AudioSource _audioSource) : base(_npc, _agent, _anim, _player, _audioSource)
    {
        name = STATE.WANDER;
        agent.speed = 2f;
    }

    public override void Enter()
    {
        previousTransform = player.transform;
        Debug.Log("isWandering");
        //Add anim

        wanderTimer = Random.Range(0f, wanderTimerMax);
        base.Enter();
    }

    public override void Update()
    {
        var type = npc.GetComponent<Enemy>().GetEnemyTypeSO();

        switch (type.enemyTypes)
        {

            case EnemyType.EnemyTypes.Manananggal:
                #region Manananggal
                if (npc.GetComponent<Enemy>().GetCollectAllItem())
                {
                    nextState = new Return(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }
                if (npc.GetComponent<Enemy>().GetBlind())
                {
                    nextState = new Fear(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (npc.GetComponent<Enemy>().GetIsStunned())
                {
                    nextState = new Stun(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                //wanderTarget += new Vector3(previousTransform.position.x * wanderJitter, 0, previousTransform.position.z * wanderJitter);
                wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);

                wanderTarget.Normalize();
                wanderTarget *= wanderRadius;

                Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDist);
                Vector3 targetWorld = npc.gameObject.transform.InverseTransformVector(targetLocal);

                Seek(targetWorld);


                wanderTimer -= Time.deltaTime;
                if (wanderTimer < 0f)
                {
                    Debug.Log("Done Wandering");
                    nextState = new Patrol(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.Mangkukulam:
                #region Mangkukulam
                wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);

                wanderTarget.Normalize();
                wanderTarget *= wanderRadius;

                targetLocal = wanderTarget + new Vector3(0, 0, wanderDist);
                targetWorld = npc.gameObject.transform.InverseTransformVector(targetLocal);

                Seek(targetWorld);
                //wanderTimer = Random.Range(5, 11);
                wanderTimer -= Time.deltaTime;
                if (wanderTimer < 0f)
                {
                    Debug.Log("Done Wandering");
                    nextState = new Return(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                break;
                #endregion
            case EnemyType.EnemyTypes.LesserFlameWisps:
                #region Lesser Flame Wisps
                wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);

                wanderTarget.Normalize();
                wanderTarget *= wanderRadius;

                targetLocal = wanderTarget + new Vector3(0, 0, wanderDist);
                targetWorld = npc.gameObject.transform.InverseTransformVector(targetLocal);

                Seek(targetWorld);
                wanderTimer -= Time.deltaTime;
                if (wanderTimer < 0f)
                {
                    nextState = new Wander(npc, agent, anim, player,audioSource);
                    stage = EVENT.EXIT;
                }

                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                break;
            #endregion

            case EnemyType.EnemyTypes.Aswang:
                #region

                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);

                wanderTarget.Normalize();
                wanderTarget *= wanderRadius;

                targetLocal = wanderTarget + new Vector3(0, 0, wanderDist);
                targetWorld = npc.gameObject.transform.InverseTransformVector(targetLocal);

                Seek(targetWorld);
                //wanderTimer = Random.Range(5, 11);
                wanderTimer -= Time.deltaTime;
                if (wanderTimer < 0f)
                {
                    Debug.Log("Done Wandering");
                    nextState = new Patrol(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                break;
            #endregion


            case EnemyType.EnemyTypes.Aswang2:
                #region

                if (npc.GetComponent<Enemy>().GetEnrage())
                {
                    nextState = new Provoke(npc, agent, anim, player, audioSource);
                    stage = EVENT.EXIT;
                }

                wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);

                wanderTarget.Normalize();
                wanderTarget *= wanderRadius;

                targetLocal = wanderTarget + new Vector3(0, 0, wanderDist);
                targetWorld = npc.gameObject.transform.InverseTransformVector(targetLocal);

                Seek(targetWorld);
                //wanderTimer = Random.Range(5, 11);
                wanderTimer -= Time.deltaTime;
                if (wanderTimer < 0f)
                {
                    Debug.Log("Done Wandering");
                    nextState = new Patrol(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }
                if (CanSeePlayer())
                {
                    nextState = new Chase(npc, agent, anim, player,audioSource);
                    previousState = this;
                    stage = EVENT.EXIT;
                }

                break;
                #endregion




        }
    }

    public override void Exit()
    {
        //reset anim
        anim.ResetTrigger("IsIdle");
        base.Exit();
    }

    private void Seek(Vector3 location)
    {
        agent.SetDestination(location);
    }

    private void Flee(Vector3 location)
    {
        Vector3 fleeVector = location - npc.transform.position;
        agent.SetDestination(npc.transform.position - fleeVector);
    }

}
