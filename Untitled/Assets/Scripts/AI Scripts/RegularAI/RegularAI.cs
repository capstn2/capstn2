﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RegularAI : MonoBehaviour
{
    private NavMeshAgent agent;

    public List<RegularAIAction> actions;
    public Transform player;
    public Animator animator;
    private Transform currentDestination;

    public float playerRange;
    public float destRange;

    public bool onPath = false;
    public bool onWait = false;
    public bool isBusy = false;
    public int index = 0;
    public int destinationIndex = 0;
    public int counter = 1;
    public int sCounter = 1;

    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        //Debug.Log("Dist = " + Vector3.Distance(player.position, this.transform.position) + " < " + playerRange);
        float dist = agent.remainingDistance;
        if (onPath)
        {
            animator.SetBool("isWalking", true);
            //Debug.Log("Remain Distance : " + GetDistanceToDestination());
            if (actions[index].waitForPlayer)
            {
                if (playerRange < Vector3.Distance(player.position, this.transform.position))
                {
                    animator.SetBool("isWalking", false);
                    agent.isStopped = true;
                    onWait = true;
                }
                else
                {
                    agent.isStopped = false;
                    onWait = false;
                }
            }
            else if (!actions[index].waitForPlayer)
            {
                onWait = false;
                agent.isStopped = false;
            }
            //Debug.Log("Path = " + agent.hasPath);
            //Debug.Log("Path = " + agent.pathStatus);
            //if (agent.remainingDistance <= 0)
            if (GetDistanceToDestination() <= destRange)
            {
                //Debug.Log("Remain Distance is Zero");
                //Debug.Log("Remain Distance : " + agent.remainingDistance);
                if (sCounter > 0)
                {
                    Debug.Log("Reached : " + actions[index].destinations[destinationIndex]);
                    destinationIndex++;
                    sCounter--;
                    onPath = false;

                    if (destinationIndex < actions[index].destinations.Count)
                    {
                        InvokeRepeating("NextDestination", 0.1f, 0.5f);
                    }
                    else if (destinationIndex >= actions[index].destinations.Count - 1)
                    {
                        onPath = false;
                        isBusy = false;
                    }
                }

                if (!isBusy)
                {
                    actions[index].ArriveDestination.Invoke();
                    destinationIndex = 0;
                    sCounter = 1;
                    index++;
                    currentDestination = null;
                }
            }
        }
        else
        {
            animator.SetBool("isWalking", false);
        }

    }

    public void OnInteract()
    {
        InvokeRepeating("FacePlayer", 0.1f, Time.deltaTime);
        if (actions[index].dialSet != null && !isBusy)
        {
            isBusy = true;
            ActivateDialogue(actions[index].dialSet);
        }
        /*
        else if (onPath)
        {
            if (actions[index].extraDialSet != null)
            {
                isBusy = true;
                ActivateDialogue(actions[index].extraDialSet);
            }
        }*/
    }

    public void ActivateDialogue(DialogueSet dialSetTL)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSetTL);
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }
    public void OnDialogueComplete()
    {
        CancelInvoke("FacePlayer");
        InvokeRepeating("OnFinishAction", 0.1f, 0.5f);
        actions[index].OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }

    public void OnFinishAction()
    {

        if (actions[index].destinations.Count > 0)
        {
            if (counter > 0)
            {
                agent.SetDestination(actions[index].destinations[destinationIndex].position);
                currentDestination = actions[index].destinations[destinationIndex];
                counter--;
            }
            //if(agent.remainingDistance > 0 && agent.pathStatus == NavMeshPathStatus.PathComplete)
            if (GetDistanceToDestination() > 0 && agent.pathStatus == NavMeshPathStatus.PathComplete)
            {
                onPath = true;
                sCounter = 1;
                counter = 1;
                CancelInvoke();
            }
        }
        else
        {
            isBusy = false;
            sCounter = 1;
            CancelInvoke();
            if (actions[index + 1] != null)
            {
                index++;
                currentDestination = null;
                destinationIndex = 0;
            }
        }
    }

    public float GetDistanceToDestination()
    {
        return Vector3.Distance(this.transform.position, currentDestination.position);
    }

    public void NextDestination()
    {
        if (actions[index].destinations[destinationIndex] != null)
        {
            if (counter > 0)
            {
                agent.SetDestination(actions[index].destinations[destinationIndex].position);
                currentDestination = actions[index].destinations[destinationIndex];
                Debug.Log("Destination : " + actions[index].destinations[destinationIndex]);
                counter--;
            }
            if (agent.remainingDistance > 0 && agent.pathStatus == NavMeshPathStatus.PathComplete)
            {
                onPath = true;
                sCounter = 1;
                counter = 1;
                CancelInvoke();
            }
        }
    }

    public void FacePlayer()
    {
        float turnSmoothTime = 2f;
        Vector3 face = this.transform.position - player.transform.position;
        var rotation = Quaternion.LookRotation(-face);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * turnSmoothTime);
    }
    public void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }
}
