﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool hasTimeLimit;
    public float timeLeft;

    public Vector3 respawnPoint;

    private void Awake()
    {
        instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        //Load is has save
        if(SaveManager.instance.hasLoaded)
        {
            //respawnPoint = SaveManager.instance.activeSave.respawnPosition;
        }
    }

    private void Update()
    {
        if (hasTimeLimit)
        {
            if (timeLeft <= 0)
            {
                SurvivedTheStage();
                SceneManagerScript.instance.ChangeSceneToDay();
            }
            else
            {
                timeLeft -= Time.deltaTime;
            }
        }
    }

    public void SurvivedTheStage()
    {
        if(SceneManagerScript.instance.ReturnSceneName() == "Manananggal")
        {
            SaveManager.instance.stageData.survivedManananggal = true;
        }
        else if(SceneManagerScript.instance.ReturnSceneName() == "Aswang")
        {
            SaveManager.instance.stageData.survivedAswang = true;
        }
    }

    public void OnPlayerDeath(GameObject player)
    {
        player.GetComponent<HealthScript>().HealUnit(player.GetComponent<HealthScript>().maxHealth);
        player.SetActive(false);
        StartCoroutine("RespawnPlayer",player);
    }

    IEnumerator RespawnPlayer(GameObject player)
    {
        yield return new WaitForSeconds(2f);
        player.SetActive(true);
        GameEvents.Instance.OnLoad();
    }
}
