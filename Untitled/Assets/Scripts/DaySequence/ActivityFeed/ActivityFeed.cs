﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivityFeed : MonoBehaviour
{
    public GameObject feedTemplate; //reference the feed prefab
    public Transform feedContainer; //used to reference the container
    public List<GameObject> feeds;

    public float feedDuration; // duration of each feed
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    public void AddFeedToActivityFeed(string feed)
    {
        GameObject fe = Instantiate(feedTemplate, feedContainer);
        fe.GetComponent<Feed>().SetReferences(this, feedDuration);
        feeds.Add(fe);
        fe.GetComponent<Text>().text = feed;
        fe.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, ((feeds.Count-1) * feedTemplate.GetComponent<RectTransform>().sizeDelta.y)*-1,0);
        feedContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(feedContainer.GetComponent<RectTransform>().sizeDelta.x,
            feeds.Count * feedTemplate.GetComponent<RectTransform>().sizeDelta.y);
    }

    public void RemoveFeedToActivityFeed(GameObject feed)
    {
        Destroy(feed.gameObject);
        feeds.Remove(feed);
        RefreshActivityFeed();
    }
    public void RefreshActivityFeed()
    {
        feedContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(feedContainer.GetComponent<RectTransform>().sizeDelta.x,
               feeds.Count * feedTemplate.GetComponent<RectTransform>().sizeDelta.y);
        for (int f = 0; f<feeds.Count;f++)
        {
            feeds[f].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, (f* feedTemplate.GetComponent<RectTransform>().sizeDelta.y) * -1, 0);
        }
    }
}
