﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feed : MonoBehaviour
{
    private float duration;
    private ActivityFeed acFeed;

    void Update()
    {
        if(duration<=0)
        {
            acFeed.RemoveFeedToActivityFeed(this.gameObject);
        }
        else
        {
            duration -= Time.deltaTime;
        }
    }

    public void SetReferences(ActivityFeed activityF, float dur)
    {
        acFeed = activityF;
        duration = dur;
    }

}
