﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayAswangCanvas : MonoBehaviour
{
    //New
    [Header("Prototyping")]
    public List<AswangInfo> aswangInfos2;
    public PlayerDataDaySequence dataPlayD;
    [Header("MainPage")]
    public GameObject aswangJournal;
    public bool isOpen;

    [Header("NeedReference")]
    public Image aswangPortrait;
    public Text aswangName;
    public Text obsContent;
    public Transform obsContainer;
    public GameObject observations;

    public List<GameObject> obsButList;
    private int currentPage = 0;


    private void Start()
    {
        if (aswangPortrait == null || aswangName == null || obsContent == null || obsContainer == null || observations == null || aswangJournal == null)
        {
            Debug.LogError("Aswang Canvas is Missing Reference/s");
        }
        else
        {
            isOpen = false;
        }
    }

    private void Update()
    {
    }

    public void ShowOrCloseAswangJournal()
    {
        if (isOpen)
        {
            ClearAswangObservationButtons();
            CloseAswangCanvas();
        }
        else
        {
            ViewAswangInformation();
            isOpen = true;
        }
    }

    public void ViewAswangInformation()
    {
        if (aswangInfos2.Count > 0)
        {
            ClearAswangObservationButtons();
            aswangPortrait.sprite = aswangInfos2[currentPage].aswangImage;
            aswangName.text = aswangInfos2[currentPage].aswangName;
            obsContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(obsContainer.GetComponent<RectTransform>().sizeDelta.x, aswangInfos2[currentPage].observations.Count * 55);
            switch (aswangInfos2[currentPage].aswangName)
            {
                case "Manananggal":
                    for (int i = 0; i < dataPlayD.manangDayObs.Count; i++)
                    {
                        GameObject but = Instantiate(observations, obsContainer);
                        but.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, i * -55, 0);
                        but.GetComponent<AswObsButton>().obsContentUI = obsContent;
                        but.GetComponent<AswObsButton>().content = dataPlayD.manangDayObs[i].obsContent;
                        but.GetComponent<AswObsButton>().obsTitleUI.text = dataPlayD.manangDayObs[i].obsTitle;
                        obsButList.Add(but);
                    }
                    break;
                case "Aswang":
                    for (int i = 0; i < dataPlayD.aswangDayObs.Count; i++)
                    {
                        GameObject but = Instantiate(observations, obsContainer);
                        but.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, i * -55, 0);
                        but.GetComponent<AswObsButton>().obsContentUI = obsContent;
                        but.GetComponent<AswObsButton>().content = dataPlayD.aswangDayObs[i].obsContent;
                        but.GetComponent<AswObsButton>().obsTitleUI.text = dataPlayD.aswangDayObs[i].obsTitle;
                        obsButList.Add(but);
                    }
                    break;
            }

        }
    }
    public void CloseAswangCanvas()
    {
        currentPage = 0;
        isOpen = false;
    }
    public void ClearAswangObservationButtons()
    {
        if (obsButList.Count > 0)
        {
            for (int d = 0; d < obsButList.Count; d++)
            {
                Destroy(obsButList[d].gameObject);
            }
            obsButList.Clear();
        }
    }
    public void PageChangeButton(int pageAdd)
    {
        if (aswangInfos2.Count <= 0)
        {
            return;
        }
        else if (aswangInfos2.Count == 1)
        {
            currentPage = 0;
        }
        else if (aswangInfos2.Count > 1)
        {
            if (currentPage + pageAdd < 0)
            {
                currentPage = aswangInfos2.Count - 1;
            }
            else if (currentPage + pageAdd > aswangInfos2.Count - 1)
            {
                currentPage = 0;
            }
            else
            {
                currentPage += pageAdd;
            }
        }
        ViewAswangInformation();
    }

    //Function for resetting Aswang canvas, used for chaning tabs
    public void ResetNotesCanvas()
    {
        ClearAswangObservationButtons();
        CloseAswangCanvas();
    }
}
