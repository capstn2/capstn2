﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManananggalPlace : MonoBehaviour
{
    public Item ash;
    public ActivityFeed acFeed;

    public PlayerDataDaySequence playDay;
    [Header("Actions")]
    public GameObject container;
    public GameObject explore;
    public GameObject investigate;
    public GameObject toolShed;
    public GameObject travel;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OpenManangPlaceActions()
    {
        container.SetActive(true);
        travel.SetActive(true);
        if (SaveManager.instance.stageData.survivedManananggal)
        {
            explore.SetActive(true);
            investigate.SetActive(true);
            toolShed.SetActive(true);
        }
        else
        {
            explore.SetActive(false);
            investigate.SetActive(false);
            toolShed.SetActive(false);
        }
    }

    public void CloseManangPlaceActions()
    {
        explore.SetActive(false);
        investigate.SetActive(false);
        toolShed.SetActive(false);
        travel.SetActive(false);
        container.SetActive(false);
    }

    public void ExploreTheAbandonedBuildings()
    {
        playDay.ReturnInventory().AddItem(ash);
        acFeed.AddFeedToActivityFeed("Explored the Buildings and Found an Ash");
    }
    public void InvestigateBloodTrails()
    {
        if (!SaveManager.instance.stageData.investigatedBloodTrails)
        {
            AswangObservation asObs = new AswangObservation();
            asObs.obsTitle = "Why?";
            asObs.obsContent = "According to the blood trails the manananggal seems to keep coming back from that old bodega. I must investigate that place";
            playDay.aswangDayObs.Add(asObs);
        }
        acFeed.AddFeedToActivityFeed("Investigated the Trails and Found an Observation");
    }
    public void InvestigateToolShed()
    {
        if(!SaveManager.instance.stageData.hasLadder)
        {
            SaveManager.instance.stageData.hasLadder = true;
        }
        acFeed.AddFeedToActivityFeed("Acquired a Ladder");
    }

    public void GoToManananggalLevel()
    {
        playDay.SaveData();
        this.gameObject.GetComponent<GateScene>().ChangeScene();
    }
}
