﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AswangPlace : MonoBehaviour
{
    public ActivityFeed acFeed;

    public GameObject container;
    public GameObject scout;
    public GameObject seal;
    public GameObject travel;

    public PlayerDataDaySequence playDay;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OpenAswangPlaceActions()
    {
        container.SetActive(true);
        travel.SetActive(true);
        if (SaveManager.instance.stageData.survivedAswang)
        {
            scout.SetActive(true);
            seal.SetActive(true);
        }
        else
        {
            scout.SetActive(false);
            seal.SetActive(false);
        }
    }

    public void CloseAswangPlaceActions()
    {
        scout.SetActive(false);
        seal.SetActive(false);
        travel.SetActive(false);
        container.SetActive(false);
    }

    public void ScoutArea()
    {
        if(!SaveManager.instance.stageData.scoutedTheAswangArea)
        {
            SaveManager.instance.stageData.scoutedTheAswangArea = true;
            AswangObservation asObs = new AswangObservation();
            asObs.obsTitle = "dsada";
            asObs.obsContent = "sdasdasdasd";
            SaveManager.instance.playerData.aswang.observations.Add(asObs);
        }
        acFeed.AddFeedToActivityFeed("Scouted The Area and Found an Observation");
    }
    public void SealTheGraves()
    {
        if(!SaveManager.instance.stageData.blessedTheGraves)
        {
            SaveManager.instance.stageData.blessedTheGraves = true;
        }
        acFeed.AddFeedToActivityFeed("Blessed the Cemeteries Graves");
    }
    public void GoToAswangLevel()
    {
        playDay.SaveData();
        this.gameObject.GetComponent<GateScene>().ChangeScene();
    }
}
