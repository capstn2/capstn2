﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BaryoPlace : MonoBehaviour
{

    public UnityEvent OnFinishDialogue;
    public GameObject aswangLevel;
    public GameObject manananggalLevel;

    public ActivityFeed acFeed;

    public DialogueSet MenFamDial;
    public DialogueSet GarFamDial;

    [Header("ObjectiveSlots")]
    public ObjectiveSlot talkToGarcia;
    public ObjectiveSlot talkToMendoza;
    [Header("Actions")]
    public GameObject container;

    void Start()
    {
    }

    
    void Update()
    {
    }
    public void OpenBaryoPlaceActions()
    {
        container.SetActive(true);
    }

    public void CloseBaryoPlaceActions()
    {
        container.SetActive(false);
    }
    private void CheckPlayerDiscoveredLocations()
    {
        if(SaveManager.instance.daySequenceData.talkedToGarcia)
        {
            aswangLevel.SetActive(true);
        }
        else
        {
            aswangLevel.SetActive(false);
        }
        if (SaveManager.instance.daySequenceData.talkedToMendoza)
        {
            manananggalLevel.SetActive(true);
        }
        else
        {
            manananggalLevel.SetActive(false);
        }
    }

    public void TalkToGarciaFam()
    {
        if(!SaveManager.instance.daySequenceData.talkedToGarcia)
        {
            ActivateDialogue(GarFamDial);
            SaveManager.instance.daySequenceData.talkedToGarcia = true;
            //ObjectiveHandler.instance.curObjectives.Add();
            aswangLevel.SetActive(true);
            ObjectiveHandler.instance.curObjectives.Add(new Objective(talkToGarcia));
        }
        acFeed.AddFeedToActivityFeed("Talked To Garcia Family");
    }
    public void TalkToMendozaFam()
    {
        if (!SaveManager.instance.daySequenceData.talkedToMendoza)
        {
            ActivateDialogue(MenFamDial);
            SaveManager.instance.daySequenceData.talkedToMendoza = true;
            manananggalLevel.SetActive(true);
            ObjectiveHandler.instance.curObjectives.Add(new Objective(talkToMendoza));
        }
        acFeed.AddFeedToActivityFeed("Talked To Mendoza Family");
    }
    public void CheckTravHouseGuide()
    {
        if (!SaveManager.instance.daySequenceData.checkedTravelHouse)
        {
            SaveManager.instance.daySequenceData.checkedTravelHouse = true;
        }
        acFeed.AddFeedToActivityFeed("Checked Travel House Guide");
    }

    public void ActivateDialogue(DialogueSet dialSetTL)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSetTL);
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }
    public void OnDialogueComplete()
    {
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }
    public void Disable()
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }


}
