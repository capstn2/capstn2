﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotkeyManager : MonoBehaviour
{
    public KeyCode journal;
    public bool canUseJournal;
    public KeyCode flashLight;
    public bool canUseFlashLight;
    public KeyCode reloadFlashLight;
    public bool canRealodFlashlight;
    public KeyCode inventory;
    public bool canUseInventory;
    public KeyCode interact;
    public bool canInteract;
    public KeyCode map;
    public bool canUseMap;
    [Header("LoadoutHotkeys")]
    public bool canUseLoadOut;
    public KeyCode loadout1;
    public KeyCode loadout2;
    public KeyCode loadout3;
    public KeyCode loadout4;
    public KeyCode loadout5;
    public KeyCode loadout6;
    public KeyCode reloadEquippedObject;

    private void Update()
    {
        if(Input.GetKeyUp(journal)&&canUseJournal)
        {
            Referencer.Instance.journalUI.ViewOrPutAwayJournal();
        }
        if (Input.GetKeyUp(flashLight)&&canUseFlashLight)
        {
            Referencer.Instance.flashlight.FlashLightSwitch();
        }
        if (Input.GetKeyUp(reloadFlashLight)&& canRealodFlashlight)
        {
            Referencer.Instance.flashlight.ReloadFlashlight();
        }
        if (Input.GetKeyUp(inventory)&&canUseInventory)
        {
            Referencer.Instance.colBeh.ToggleInventory();
        }
        if (Input.GetKeyUp(interact)&&canInteract)
        {
            Referencer.Instance.colBeh.InteractItem();
        }
        if (Input.GetKeyUp(map)&&canUseMap)
        {
            Referencer.Instance.miniMapUI.OpenOrCloseMap();
        }

        if (Input.GetKeyUp(reloadEquippedObject))
        {
            Referencer.Instance.playerLoadout.ReloadEquipment();
        }



        if (Input.GetKeyUp(loadout1))
        {
            Referencer.Instance.playerLoadout.ChangeEquipment(0);
        }
        if (Input.GetKeyUp(loadout2))
        {
            Referencer.Instance.playerLoadout.ChangeEquipment(1);
        }
        if (Input.GetKeyUp(loadout3))
        {
            Referencer.Instance.playerLoadout.ChangeEquipment(2);
        }
        if (Input.GetKeyUp(loadout4))
        {
            Referencer.Instance.playerLoadout.ChangeEquipment(3);
        }
        if (Input.GetKeyUp(loadout5))
        {
            Referencer.Instance.playerLoadout.ChangeEquipment(4);
        }
        if (Input.GetKeyUp(loadout6))
        {
            Referencer.Instance.playerLoadout.ChangeEquipment(5);
        }


        if (Input.GetMouseButtonDown(0))
        {
            Referencer.Instance.playerLoadout.ExecuteEquipment();
        }
        if (Input.GetMouseButtonDown(1))
        {
            Referencer.Instance.playerLoadout.OnToggle();
        }
    }

}
