﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corpse : MonoBehaviour
{
    public GameObject blessedLight;
    public bool isBlessed = false;

    public void BlessTheCorpse(Inventory pInv)
    {
        if (!isBlessed)
        {
            for (int i = 0; i < pInv.GetInventoryList().Count; i++)
            {
                if (pInv.GetInventoryList()[i].interactable.itemType == Item.ItemType.TearsOfBathala)
                {
                    isBlessed = true;
                    ObjectiveEvent.Instance.EvaluateCorpseBlessed();
                    pInv.RemoveItem(pInv.GetInventoryList()[i]);
                    blessedLight.SetActive(true);
                    return;
                }
            }
            Debug.Log("NoTears");
        }
    }

}
