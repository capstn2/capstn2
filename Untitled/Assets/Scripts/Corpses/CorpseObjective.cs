﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpseObjective : MonoBehaviour
{
    public static EventHandler OnObjectiveComplete;

    public List<Corpse> corpses;
    public int questId;

    public ObjectiveSlot nextSlot;
    void Start()
    {
        ObjectiveEvent.Instance.evalCorpseBlessed += EvaluateCorpseObjective;
    }

    void Update()
    {
        
    }
    public void EvaluateCorpseObjective()
    {
        float cBlessed = 0;
        for(int c = 0;c<corpses.Count;c++)
        {
            if(corpses[c].isBlessed)
            {
                cBlessed++;
            }
        }
        Debug.Log("CorpseBlessed : " + cBlessed + " >= " + " Count : " + corpses.Count);
        if (cBlessed >= corpses.Count)
        {
            OnObjectiveComplete?.Invoke(this, EventArgs.Empty);
            CompletedObjective();
        }
    }
    public void CompletedObjective()
    {
        ObjectiveEvent.Instance.OnQuestArrivedS(questId);
        ObjectiveHandler.instance.AddObjective(nextSlot);
        Debug.Log("CompletedObjective");
    }
}
