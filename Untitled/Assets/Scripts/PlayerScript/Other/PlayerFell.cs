﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFell : MonoBehaviour
{
    public Transform player;
    public Transform currentCheckPoint;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "DeathPit")
        {
            player.GetComponent<CharacterController>().enabled = false;
            player.position = currentCheckPoint.position;
            player.GetComponent<CharacterController>().enabled = true;
            player.GetComponent<HealthScript>().DamageUnit(50);
        }
        else if (other.gameObject.tag == "CheckPoint")
        {
            currentCheckPoint = other.transform;
        }
    }
}
