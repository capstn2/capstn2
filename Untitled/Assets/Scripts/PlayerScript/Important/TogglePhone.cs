﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


public class TogglePhone : MonoBehaviour
{
    public CinemachineFreeLook cinema;
    private Transform player;
    public GameObject phone;
    private PhoneScript phoneScpt;

    private bool isToggled = false;

    public Vector2 topRig;
    public Vector2 midRig;
    public Vector2 botRig;
    public Vector2 midOffSet;
    public Vector2 topNbotOffset;
    public Vector2 xyAxisValue;

    private Vector2 ogTopRig;
    private Vector2 ogMidRig;
    private Vector2 ogBotRig;
    private Vector2 ogMidOffSet;
    private Vector2 ogTopNbotOffset;
    private float ogCineSpeed;

    void Start()
    {
        Referencer.Instance.aimToPhone = this;
        player = transform.parent;
        phoneScpt = phone.GetComponent<PhoneScript>();
        phone.SetActive(false);
        if(cinema == null||phone == null)
        {
            Debug.LogError("Cinema or Phone is Null");
        }
    }
    void Update()
    {
    }

    public void ToggleAtPhone()
    { 
        if (isToggled)
        {
            Referencer.Instance.radar.enabled = false;
            CinemachineOut();
            isToggled = false;
            phone.SetActive(false);
        }
        else
        {
            Referencer.Instance.radar.enabled = true;
            SetCinamachineValues();
            CinemachineIn();
            isToggled = true;
            phone.SetActive(true);
        }
    }

    public void ToggleRadar()
    {
        phoneScpt.ToggleRadar();
    }
    public void ReloadRadar()
    {
        phoneScpt.ReloadRadar();
    }
    public void SetCinamachineValues()
    {
        ogTopRig = new Vector2(cinema.m_Orbits[0].m_Height, cinema.m_Orbits[0].m_Radius);
        ogMidRig = new Vector2(cinema.m_Orbits[1].m_Height, cinema.m_Orbits[1].m_Radius);
        ogBotRig = new Vector2(cinema.m_Orbits[2].m_Height, cinema.m_Orbits[2].m_Radius);
        ogMidOffSet = new Vector2(cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.x,
            cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y);
        ogTopNbotOffset = new Vector2(cinema.GetRig(0).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y,
            cinema.GetRig(2).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y);
        ogCineSpeed = cinema.m_YAxis.m_MaxSpeed;
    }

    public void CinemachineIn()
    {
        cinema.m_LookAt = phone.transform;
        cinema.m_Follow = phone.transform;
        cinema.m_YAxis.m_MaxSpeed = 0;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = midOffSet.y;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.x = midOffSet.x;
        cinema.m_Orbits[0].m_Height = topRig.x;
        cinema.m_Orbits[0].m_Radius = topRig.y;
        cinema.m_Orbits[1].m_Height = midRig.x;
        cinema.m_Orbits[1].m_Radius = midRig.y;
        cinema.m_Orbits[2].m_Height = botRig.x;
        cinema.m_Orbits[2].m_Radius = botRig.y;
        cinema.m_YAxis.Value = xyAxisValue.x;
        cinema.m_XAxis.Value = xyAxisValue.y;
    }

    public void CinemachineOut()
    {

        cinema.m_YAxis.m_MaxSpeed = ogCineSpeed;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = ogMidOffSet.y;
        cinema.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.x = ogMidOffSet.x;
        cinema.GetRig(0).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = ogTopNbotOffset.x;
        cinema.GetRig(2).GetCinemachineComponent<CinemachineComposer>().m_TrackedObjectOffset.y = ogTopNbotOffset.y;
        cinema.m_LookAt = player;
        cinema.m_Follow = player;
        cinema.m_Orbits[0].m_Height = ogTopRig.x;
        cinema.m_Orbits[0].m_Radius = ogTopRig.y;
        cinema.m_Orbits[1].m_Height = ogMidRig.y;
        cinema.m_Orbits[1].m_Radius = ogMidRig.y;
        cinema.m_Orbits[2].m_Height = ogBotRig.y;
        cinema.m_Orbits[2].m_Radius = ogBotRig.y;
    }

}
