﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneScript : MonoBehaviour
{
    [Header("Important Elements")]
    [SerializeField] public LayerMask enemyMask;
    public float radMaxBatCharge;
    public float radCurBatCharge;
    public GameObject radarUI;
    public float attractRadius;
    public float batteryConsume;
    public bool isOn;
    void Start()
    {
        GameEvents.Instance.radarMakesNoise += PhoneMakeBeep;
        radarUI = Referencer.Instance.radarUI;
    }

    void Update()
    {
        if (isOn && radCurBatCharge > 0) 
        {
            radCurBatCharge -= batteryConsume * Time.deltaTime;
            Referencer.Instance.phoneUI.UpdateBatteryUI(radCurBatCharge,radMaxBatCharge);
            if (radCurBatCharge <= 0)
            {
                isOn = false;
                radarUI.SetActive(false);
            }
        }
    }


    public void ReloadRadar()
    {
        Inventory inv = Referencer.Instance.colBeh.GetInventory();
        for(int i = 0;i<inv.GetInventoryList().Count;i++)
        {
            if(inv.GetInventoryList()[i].interactable.itemType == Item.ItemType.Battery)
            {
                Item nItem = new Item();
                nItem = inv.GetInventoryList()[i].ReturnDeepCopy();
                radCurBatCharge = nItem.interactable.curBatCharge;
                radMaxBatCharge = nItem.interactable.maxBatCharge;
                inv.RemoveItem(inv.GetInventoryList()[i]);
                return;
            }
        }
    }

    public void ToggleRadar()
    {
        if(isOn)
        {
            radarUI.SetActive(false);
            isOn = false;
        }
        else
        {
            if (radCurBatCharge > 0)
            {
                radarUI.SetActive(true);
                isOn = true;
            }
        }
    }

    public void PhoneMakeBeep()
    {
        Collider[] col = Physics.OverlapSphere(this.transform.position, attractRadius, enemyMask);
        if (col.Length > 0)
        {
            for (int i = 0; i < col.Length; i++)
            {
                
            }
        }
    }
}
