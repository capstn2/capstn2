﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonMovement : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;
    public Animator animator;
    public Flashlight flashlight;

    public float wlkSpeed = 5f;
    public float sprntSpeed = 8f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    public bool isSprinting = false;
    public bool canMove = true;
    

    public GameObject instructionCanvas;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        //Debug.Log(direction);


        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isSprinting = true;
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            isSprinting = false;
        }
        if (direction.magnitude>=0.1f&&canMove)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg+cam.eulerAngles.y; //Atan2 is to get the angle based on the characters direction.
            //Debug.Log("TAngle : " + targetAngle);
            if(!isSprinting)
            {
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, cam.eulerAngles.y, ref turnSmoothVelocity, turnSmoothTime); //this is to smoothen the rotation.
                transform.rotation = Quaternion.Euler(0f, angle, 0f);// executes the rotation.

                //Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;//gets the direction based on the atan2
                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                controller.Move(new Vector3(moveDir.x, -1f, moveDir.z).normalized * wlkSpeed * Time.deltaTime);//executes movement
            }
            else if(isSprinting)
            {
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
                //controller.Move(moveDir.normalized * sprntSpeed * Time.deltaTime);
                controller.Move(new Vector3(moveDir.x, -1f, moveDir.z).normalized * sprntSpeed * Time.deltaTime);
            }

        }
        else
        {
            controller.Move(new Vector3(0f, -1f, 0f).normalized * sprntSpeed * Time.deltaTime);
        }
        HandleAnimation(direction);
    }

    public void CanPlayerMove(bool resMove)
    {
        canMove = resMove;
    }

    public void HandleAnimation(Vector3 dir)
    {

        if (canMove)
        {
            animator.SetFloat("Speed", dir.magnitude);
            if (isSprinting)
            {
                animator.SetFloat("Speed", dir.magnitude);
                animator.SetBool("isSprinting", true);
                animator.SetBool("isStrafe", false);
            }
            else
            {
                animator.SetBool("isSprinting", false);
                if (dir.z != 0 && dir.x != 0)
                {
                    if ((dir.z >= .7 && dir.x >= .7) || (dir.z <= -0.7 && dir.x >= 0.7))
                    {
                        animator.SetFloat("Speed", 1);
                        animator.SetBool("isStrafe", true);
                    }
                    else
                    {
                        animator.SetFloat("Speed", -1);
                        animator.SetBool("isStrafe", true);
                    }
                }
                else if (dir.z > 0.1)
                {
                    animator.SetFloat("Speed", dir.z);
                    animator.SetBool("isStrafe", false);
                }
                else if (dir.z < -0.1)
                {
                    animator.SetFloat("Speed", dir.z);
                    animator.SetBool("isStrafe", false);
                }
                else if (dir.x > 0.1 && !isSprinting)
                {
                    animator.SetFloat("Speed", dir.x);
                    animator.SetBool("isStrafe", true);
                }
                else if (dir.x < -0.1 && !isSprinting)
                {
                    animator.SetFloat("Speed", dir.x);
                    animator.SetBool("isStrafe", true);
                }
                else
                {
                    animator.SetFloat("Speed", dir.magnitude);
                    animator.SetBool("isStrafe", false);
                }
            }
        }
        else
        {
            animator.SetFloat("Speed", 0);
            animator.SetBool("isStrafe", false);
            animator.SetBool("isSprinting", false);
        }
    }


   
}
