﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerInteract : MonoBehaviour
{
    private float interactRange = 5f;
    private Transform itemRayCast;
    [SerializeField] Transform questPrompt;

    [SerializeField] bool hasAsh = false;
    [SerializeField] bool hasGarlic =false;
    [SerializeField] bool hasSalt =false;


    private void Awake()
    {

        itemRayCast = transform.Find("itemRayCast");
        //questPrompt = transform.Find("questPrompt");
    }

    // Start is called before the first frame update
    void Start()
    {
      

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(hasAll());
        RaycastHit prompt;
        if (Physics.Raycast(new Vector3(itemRayCast.position.x, itemRayCast.position.y, itemRayCast.position.z), itemRayCast.forward, out prompt, interactRange))
        {

            var enemyInteract = prompt.collider.GetComponent<EnemyInteract>();
            if(enemyInteract != null && hasAll() == false)
            {
                Debug.Log("hit enemy body");
                questPrompt.gameObject.SetActive(true);
                var text = questPrompt.Find("text");
                text.GetComponent<TextMeshProUGUI>().text = "You dont have the required items yet \n ASH \n GARLIC \n SALT";
            }else if(enemyInteract != null && hasAll())
            {
                questPrompt.gameObject.SetActive(true);
                var text = questPrompt.Find("text");
                text.GetComponent<TextMeshProUGUI>().text = "E - Destroy the body";
            }

        }
        else
        {
            questPrompt.gameObject.SetActive(false);
        }
       




    }


    public bool hasAll()
    {
        if(hasAsh && hasGarlic && hasSalt)
        {
            return true;
        }
        return false;
    }


    public bool GetAsh()
    {
        return hasAsh;
    }

    public void SetAsh(bool value)
    {
        hasAsh = value;
    }

    public bool GetGarlic()
    {
        return hasGarlic;
    }

    public void SetGarlic(bool value)
    {
        hasGarlic = value;
    }

    public bool GetSalt()
    {
        return hasSalt;
    }

    public void SetSalt(bool value)
    {
        hasSalt = value;
    }
}
