﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataDaySequence : MonoBehaviour
{
    [SerializeField] private DayUiInventory uiInventory;

    public Inventory dayInventory;
    public List<PlayerNotes> dayNotes;
    public List<AswangObservation> manangDayObs;
    public List<AswangObservation> aswangDayObs;
    private void Awake()
    {
        dayInventory = new Inventory(UseItem);
    }
    void Start()
    {
        uiInventory.SetInventory(dayInventory);
        uiInventory.Hide();
        LoadSaveHere();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UseItem(Item item)
    {

    }

    public void ToggleInventory()
    {
        if (uiInventory.GetIsOpen() == true)
        {
            uiInventory.Hide();
            uiInventory.SetIsOpen(false);
        }
        else if (uiInventory.GetIsOpen() == false)
        {
            uiInventory.Show();
            uiInventory.SetIsOpen(true);
        }
    }

    public Inventory ReturnInventory()
    {
        return dayInventory;
    }

    public void LoadSaveHere()
    {
        ////ForNotes
        List<PlayerNotes> dNCopy = new List<PlayerNotes>();
        for(int n = 0;n<SaveManager.instance.playerData.playerNotes.Count;n++)
        {
            dNCopy.Add(SaveManager.instance.playerData.playerNotes[n].ReturnDeepCopyNotes());
        }
        dayNotes = dNCopy;
        ////ForInventory
        List<Item> dICopy = new List<Item>();
        for(int i = 0; i< SaveManager.instance.playerData.itemList.Count;i++)
        {
            dICopy.Add(SaveManager.instance.playerData.itemList[i].ReturnDeepCopy());
        }
        dayInventory.ChangeInvItemList(dICopy);
        uiInventory.RefreshInventoryItems();
        //Observations
        List<AswangObservation> manangDayCopy = new List<AswangObservation>();
        for(int m = 0;m< SaveManager.instance.playerData.mananananggalObs.Count;m++)
        {
            manangDayCopy.Add(SaveManager.instance.playerData.mananananggalObs[m].ReturnDeepCopyObs());
        }
        manangDayObs = manangDayCopy;
        //Aswang
        List<AswangObservation> aswangDayCopy = new List<AswangObservation>();
        for (int m = 0; m < SaveManager.instance.playerData.aswangObs.Count; m++)
        {
            aswangDayCopy.Add(SaveManager.instance.playerData.aswangObs[m].ReturnDeepCopyObs());
        }
        aswangDayObs = aswangDayCopy;
    }

    public void SaveData()
    {
        ////ForNotes
        List<PlayerNotes> dNCopy = new List<PlayerNotes>();
        for (int n = 0; n < dayNotes.Count; n++)
        {
            dNCopy.Add(dayNotes[n].ReturnDeepCopyNotes());
        }
        SaveManager.instance.playerData.playerNotes = dayNotes;
        ////ForInventory
        List<Item> dICopy = new List<Item>();
        for (int i = 0; i < dayInventory.GetInventoryList().Count; i++)
        {
            dICopy.Add(dayInventory.GetInventoryList()[i].ReturnDeepCopy());
        }
        SaveManager.instance.playerData.itemList = dICopy;
        //Observations
        List<AswangObservation> manangDayCopy = new List<AswangObservation>();
        for (int m = 0; m < manangDayObs.Count; m++)
        {
            manangDayCopy.Add(manangDayObs[m].ReturnDeepCopyObs());
        }
        SaveManager.instance.playerData.mananananggalObs = manangDayCopy;
        //Aswang
        List<AswangObservation> aswangDayCopy = new List<AswangObservation>();
        for (int m = 0; m < aswangDayObs.Count; m++)
        {
            aswangDayCopy.Add(aswangDayObs[m].ReturnDeepCopyObs());
        }
        SaveManager.instance.playerData.aswangObs = aswangDayCopy;
    }

}
