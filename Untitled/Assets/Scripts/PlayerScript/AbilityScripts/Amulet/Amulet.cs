﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Amulet : ScriptableObject
{
    public string sName;
    public float aCooldown;
    public float dCooldown;
    public bool canUseAblty;
    public Item.ItemType whatAmulet;

    public abstract void GetPlayerAbilities(GameObject obj);//To Get a Reference Object
    public abstract void TriggerScroll();//Execute Skill
}
