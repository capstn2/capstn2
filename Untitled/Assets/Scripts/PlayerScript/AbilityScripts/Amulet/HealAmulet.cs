﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Amulet/HealAmulet")]
public class HealAmulet : Amulet
{
    public GameObject amuletEffect;
    private Transform origin;
    public override void GetPlayerAbilities(GameObject obj)
    {
        origin = obj.transform;
        TriggerScroll();
    }

    public override void TriggerScroll()
    {
        GameObject heal = Instantiate(amuletEffect, origin.parent.position, origin.rotation);
        heal.GetComponent<Heal>().SetPrivates(origin.parent.GetComponent<HealthScript>(), origin.parent);
    }
}
