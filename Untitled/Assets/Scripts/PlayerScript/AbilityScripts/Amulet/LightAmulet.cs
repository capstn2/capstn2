﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Amulet/LightAmulet")]
public class LightAmulet : Amulet
{
    public GameObject amuletEffect;
    private Transform origin;
    public override void GetPlayerAbilities(GameObject obj)
    {
        origin = obj.transform;
        TriggerScroll();
    }

    public override void TriggerScroll()
    {
        GameObject light = Instantiate(amuletEffect, origin.position, origin.rotation);
        light.GetComponent<LightEffect>().SetPosition();
    }
}
