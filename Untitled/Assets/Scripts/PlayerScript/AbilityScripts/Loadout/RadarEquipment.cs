﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Loadout/Radar")]
public class RadarEquipment : EquipableLoadout
{
    public override void GetReference(GameObject refer)
    {

    }

    public override void OnEquipped()
    {
        Referencer.Instance.aimToPhone.ToggleAtPhone();
    }

    public override void OnExecute()
    {
        Referencer.Instance.aimToPhone.ToggleRadar();
    }

    public override void OnUnEquipped()
    {
        Referencer.Instance.aimToPhone.ToggleAtPhone();
    }

    public override void OnUse()
    {
    }

    public override void OnReloadEquipment()
    {
        Referencer.Instance.aimToPhone.ReloadRadar();
    }
}
