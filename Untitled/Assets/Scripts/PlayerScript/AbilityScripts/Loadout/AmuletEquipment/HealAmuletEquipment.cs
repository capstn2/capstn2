﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Loadout/HealAmulet")]
public class HealAmuletEquipment : EquipableLoadout
{
    private GameObject origin;
    private Amulet whatAmulet;
    public int scrollID;
    public override void GetReference(GameObject refer)
    {
        origin = refer;
        PlayerLoadout pl = refer.GetComponent<PlayerLoadout>();
        whatAmulet = pl.healAmulet;
    }

    public override void OnEquipped()
    {
    }

    public override void OnExecute()
    {
        if (CanUseAmulet() || origin.GetComponent<PlayerLoadout>().isTesting)
        {
            whatAmulet.GetPlayerAbilities(origin);
        }
    }

    public bool CanUseAmulet()
    {
        Inventory inv = origin.GetComponent<PlayerLoadout>().ReturnInventory();
        for (int i = 0; i < inv.GetInventoryList().Count; i++)
        {
            if (whatAmulet.whatAmulet == inv.GetInventoryList()[i].interactable.itemType)
            {
                inv.RemoveItem(inv.GetInventoryList()[i]);
                return true;
            }
        }
        return false;
    }
    public override void OnUnEquipped()
    {
    }

    public override void OnUse()
    {
    }

    public override void OnReloadEquipment()
    {
    }
}
