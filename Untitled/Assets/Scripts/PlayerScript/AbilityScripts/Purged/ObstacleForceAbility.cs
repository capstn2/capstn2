﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Abilities/ObstacleForceAbility")]
public class ObstacleForceAbility : Ability
{
    public LayerMask maskLayer;
    public LayerMask highLightMask;
    public Material highLight;
    private Transform cam;
    private PlayerLoadout ablty;
    public override void GetPlayerAbilities(GameObject obj)
    {

        //cam = obj.GetComponent<ThirdPersonMovement>().cam.transform;
        cam = obj.transform.parent.GetComponent<ThirdPersonMovement>().cam.transform;
        ablty = obj.GetComponent<PlayerLoadout>();
        if (ablty.isBusy)
        {
            Transform objHand = obj.gameObject.transform.parent.Find("ObjectHandler");
            if (objHand.GetComponent<ObstacleForceScript>().canPlace)
            {
                //ablty.PlaceObstacleForce();//Purged
                canUseAblty = false;
            }
            //Debug.Log("Can be used = " + canUseAblty);
        }
        else
        {
            //ablty.playerMana.EnoughToUseAbility(this);
            TriggerAbility();
        }
    }

    public override void PassiveAbility(GameObject obj)
    {
        if (!obj.GetComponent<PlayerLoadout>().isBusy)
        {
            //obj.GetComponent<PlayerAbilities>().HighLightObject(highLight, highLightMask);
        }
    }

    public override void TriggerAbility()
    {
        //ablty.ObstacleForce(cam, maskLayer); // purged
    }
}
