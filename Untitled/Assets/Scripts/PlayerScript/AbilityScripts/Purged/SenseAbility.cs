﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName ="Abilities/SenseAbility")]
public class SenseAbility : Ability
{
    private PlayerLoadout ablty;

    public override void GetPlayerAbilities(GameObject obj)
    {
        ablty = obj.GetComponent<PlayerLoadout>();
        TriggerAbility();

    }

    public override void PassiveAbility(GameObject obj)
    {
    }

    public override void TriggerAbility()
    {
        canUseAblty = false;
        ablty.CastSense();
    }
}
