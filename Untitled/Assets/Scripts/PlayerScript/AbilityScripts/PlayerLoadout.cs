﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLoadout : MonoBehaviour
{
    private ManaScript playerMana;
    private HealthScript playerHealth;


    public bool isBusy = false;
    public bool isTesting = false;

    //public GameObject highLighted;
    //public Material hLightedOrigMat;
    private CollectBehaviour colBeh;
    private Inventory pInventory;
    public GameObject amuletPoint;

    public Amulet fireballAmulet;
    public Amulet freezeAmulet;
    public Amulet healAmulet;
    public Amulet lightAmulet;

    [Tooltip("In Right hand Mixamo Rig")] public Transform firePointAmulet;
    [Tooltip("Crosshair Target in Camera")] public Transform projectileDirection;
    public List<EquipableLoadout> equipment;
    public EquipableLoadout currentEquipped;
    public EquipableLoadout NoLoad;

    void Start()
    {
        Referencer.Instance.playerLoadout = this;
        colBeh = transform.parent.GetComponent<CollectBehaviour>();
        pInventory = colBeh.GetInventory();
        playerMana = transform.parent.GetComponent<ManaScript>();
        playerHealth = transform.parent.GetComponent<HealthScript>();
    }
    void Update()
    {
    }

    public void ChangeEquipment(int i)
    {
        if(currentEquipped == equipment[i])
        {
            currentEquipped.OnUnEquipped();
            currentEquipped = NoLoad;
        }
        else if(i+1>equipment.Count)
        {
            Debug.Log("Equipment Unavailable");
        }
        else
        {
            if (currentEquipped != null)
            {
                currentEquipped.OnUnEquipped();
            }
            currentEquipped = equipment[i];
            currentEquipped.GetReference(this.gameObject);
            currentEquipped.OnEquipped();
        }
    }
    public void ReloadEquipment()
    {
        currentEquipped.OnReloadEquipment();
    }

    public void ExecuteEquipment()
    {
        currentEquipped.OnExecute();
    }

    public void OnToggle()
    {
        currentEquipped.OnUse();
    }

    public Inventory ReturnInventory()
    {
        return pInventory;
    }
    /*
    public void CastHeal(GameObject particle, GameObject prefab, float amount, float duration)
    {
        GameObject hPrefab = Instantiate(prefab, this.transform.position, this.transform.rotation, this.transform);
        hPrefab.GetComponent<HealAbilityScript>().Constructer(duration,amount,this.transform.parent.gameObject,particle);

        //StartCoroutine(HealUnit(particle));
        this.transform.parent.gameObject.GetComponent<ThirdPersonMovement>().canMove = false;
        isBusy = true;
    }
    
    public void HighLightObject(Material hiLiMaterial, LayerMask maskLayer)
    {
        //Transform theCam = this.transform.parent.GetComponent<ThirdPersonMovement>().cam.transform;
        Transform theCam = Camera.main.transform;
        RaycastHit hit;
        Physics.Raycast(theCam.transform.position, theCam.forward, out hit, 50f, maskLayer);
        Transform objHand = this.gameObject.transform.parent.Find("ObjectHandler");
        ObstacleForceScript scr = objHand.GetComponent<ObstacleForceScript>();
        if (hit.collider && hit.collider.GetComponent<ObsForceObj>())
        {
            ObsForceObj objForceScrpt = hit.collider.GetComponent<ObsForceObj>();

            if (hit.collider && highLighted == null)
            {
                objForceScrpt.beingPointed = true;
                highLighted = hit.collider.gameObject;
            }
            else if (hit.collider && hit.collider.gameObject == highLighted.gameObject)
            {

            }
        }
        else if (highLighted != null)
        {
            highLighted.GetComponent<ObsForceObj>().ChangeMatOriginal();
            highLighted.GetComponent<ObsForceObj>().beingPointed = false;
            highLighted = null;
        }
    }    
    */

    public void CastSense()
    {
        StartCoroutine("SenseUnit");
    }


    IEnumerator SenseUnit()
    {
        Transform senseAbility = this.transform.parent.Find("pfSensePulse");
        SenseAbilityBehaviour behaviour = senseAbility.GetComponent<SenseAbilityBehaviour>();
        behaviour.Show();
        yield return new WaitForSeconds(3f);
        behaviour.Hide();

    }

    public void CastBless()
    {
        StartCoroutine("BlessUnit");
    }

    IEnumerator BlessUnit()
    {
        Transform blessAbility = this.transform.parent.Find("pfBlessEffect");
        BlessAbilityBehaviour behaviour = blessAbility.GetComponent<BlessAbilityBehaviour>();
        behaviour.Show();
        yield return new WaitForSeconds(3f);
        behaviour.Hide();
    }

}
