﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using System;

public class Freeze : MonoBehaviour
{
    public static EventHandler OnFreezeHit;
    public static EventHandler OnFreezeShot;

    public float duration;
    public float radius;
    public Vector3 direction;
    public Vector3 playerDirection;
    private Vector3 particleDirection;
    private Vector3 particleSpeed = new Vector3(35,35,35);
    public VisualEffect vfx;
    [SerializeField] public LayerMask maskLayer;

    private void OnDrawGizmos()
    {
        Vector3 p1 = this.transform.position;
        Vector3 p2 = p1 + this.transform.forward * 15;
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(p1, radius);


        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(p2, radius);
    }

    void Start()
    {

        OnFreezeShot?.Invoke(this, EventArgs.Empty);
        this.transform.eulerAngles = playerDirection;
        //particleDirection = new Vector3(direction.x*particleSpeed.x, direction.y*particleSpeed.y , direction.z*particleSpeed.z);
        //vfx.SetVector3("Force", particleDirection);
    }

    public void FreezeEnemies()
    {
        Vector3 p1 = this.transform.position;
        Vector3 p2 = p1 + this.transform.forward * 15;
        RaycastHit[] hits = Physics.CapsuleCastAll(p1, p2, radius, this.transform.right, 15, maskLayer);
        Debug.DrawLine(this.transform.position, this.transform.forward * 15, Color.green);
        //Debug.Log(hits.Length);
        if (hits.Length>0)
        {
            for (int i = 0;i<hits.Length;i++)
            {
                Debug.Log("Name : "+hits[i].collider.name);
                if (hits[i].collider.gameObject.GetComponent<ApplyFrost>())
                {
                    Debug.Log("HasFrost");
                    hits[i].collider.gameObject.GetComponent<ApplyFrost>().GetFrosted();
                }

                foreach (RaycastHit c in hits)
                {
                    if (c.collider.GetComponent<Enemy>())
                    {
                        OnFreezeHit?.Invoke(this, EventArgs.Empty);
                    }
                }
            }
        }    
    }

    void Update()
    {
        FreezeEnemies();
        if (duration > 0)
        {
            duration -= Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
