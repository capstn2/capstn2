﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlayerType/Type")]
public class PlayerType : ScriptableObject
{
    public string typeName = "type name";
    public List<Ability> abilities;

}
