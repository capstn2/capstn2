﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersData : MonoBehaviour
{
    public List<PlayerNotes> pNotes;

    public AswangInfo manananggal;
    public AswangInfo aswang;
    [Header("Manananggal Observation")]
    public AswangObservation manangPlayLH;
    public AswangObservation manangPlayNoLB;
    public AswangObservation mangPlayDiscoverWeak;
    [Header("Aswang Observation")]
    public AswangObservation aswangInvisible;
    public AswangObservation aswangfamilyEstate;
    public AswangObservation aswangEatCorpes;
    public AswangObservation aswangWeakness;

    [Header("Players Observations")]
    public List<AswangObservation> manangObservations;
    public List<AswangObservation> aswangObservations;
    [Header("Audio")]
    public AudioClip scribble;

    void Start()
    {
        Referencer.Instance.pDatas = this;
        GivePlayerDatas();

        AswangObsEvent.Instance.onManananggalSeePlayerLH += ManananggalSeePlayerLowHealth;
        AswangObsEvent.Instance.onPlayerSeeManananggalNoLB += PlayerDontSeeManangLowerBody;
        AswangObsEvent.Instance.onPlayerSeeManananggalWeakN += PlayerDiscoverMangWeakness;

        AswangObsEvent.Instance.onPlayerSawAswangInvi += PlayerSawAswangGoInvi;
        AswangObsEvent.Instance.onPlayerEnterFamilyEstate += PlayerEnteredFamilyEstate;
        AswangObsEvent.Instance.onPlayerSeeAswangEatCorpse += PlayerSawAswangEatingCorpses;
        AswangObsEvent.Instance.onPlayerDiscoverWeakness += PlayerDiscoverAswangWeakness;

        GameEvents.Instance.onSave += SavePlayer;
        GameEvents.Instance.onLoad += LoadPlayer;
        GameEvents.Instance.clearListeners += ClearThisListeners;
    }

    
    private void GivePlayerDatas()
    {
        if(SaveManager.instance.playerData.itemList.Count>0)
        {
            GivePlayerInventory();
        }
        if(SaveManager.instance.playerData.playerNotes.Count>0)
        {
            GivePlayerNotes();
        }
    }
    public void SavePlayerInventory()
    {
        CollectBehaviour colBeh = this.GetComponent<CollectBehaviour>();
        List<Item> itemListCopy = new List<Item>();
        for (int c = 0; c < colBeh.GetInventory().GetInventoryList().Count; c++)
        {
            itemListCopy.Add(colBeh.GetInventory().GetInventoryList()[c].ReturnDeepCopy());
        }
        SaveManager.instance.playerData.itemList = itemListCopy;
    }
    public void SavePlayerNotesAndObservations()
    {
        ///SavePlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesSave = new List<PlayerNotes>();
        for (int n = 0; n < pData.pNotes.Count; n++)
        {
            pNotesSave.Add(pData.pNotes[n].ReturnDeepCopyNotes());
        }
        SaveManager.instance.playerData.playerNotes = pNotesSave;
        ///////////////////
        ///ManananggalObs
        List<AswangObservation> copyManang = new List<AswangObservation>();
        for (int m = 0; m < manangObservations.Count; m++)
        {
            copyManang.Add(manangObservations[m].ReturnDeepCopyObs());
        }
        SaveManager.instance.playerData.mananananggalObs = copyManang;
        //AswangObs
        List<AswangObservation> copyAswang = new List<AswangObservation>();
        for (int a = 0; a < aswangObservations.Count; a++)
        {
            copyAswang.Add(aswangObservations[a].ReturnDeepCopyObs());
        }
        SaveManager.instance.playerData.aswangObs = copyAswang;
    }
    public void GivePlayerInventory()
    {
        CollectBehaviour colBeh = this.GetComponent<CollectBehaviour>(); 
        colBeh.GetInventory().ChangeInvItemList(SaveManager.instance.playerData.itemList);
        colBeh.ReturnUIInventory().RefreshInventoryItems();
    }
    public void GivePlayerNotes()
    {
        pNotes = SaveManager.instance.playerData.playerNotes;
    }
    public List<PlayerNotes> ReturnNotes()
    {
        return pNotes;
    }
    public void AddPlayerNotes(PlayerNotes notes)
    {
        pNotes.Add(notes);
    }

    public void ClearThisListeners()
    {
        GameEvents.Instance.onSave -= SavePlayer;
        GameEvents.Instance.onLoad -= LoadPlayer;
        GameEvents.Instance.clearListeners -= ClearThisListeners;
        AswangObsEvent.Instance.onManananggalSeePlayerLH -= ManananggalSeePlayerLowHealth;
        AswangObsEvent.Instance.onPlayerSeeManananggalNoLB -= PlayerDontSeeManangLowerBody;
        AswangObsEvent.Instance.onPlayerSeeManananggalWeakN -= PlayerDiscoverMangWeakness; 
        AswangObsEvent.Instance.onPlayerSawAswangInvi -= PlayerSawAswangGoInvi;
        AswangObsEvent.Instance.onPlayerEnterFamilyEstate -= PlayerEnteredFamilyEstate;
        AswangObsEvent.Instance.onPlayerSeeAswangEatCorpse -= PlayerSawAswangEatingCorpses;
        AswangObsEvent.Instance.onPlayerDiscoverWeakness -= PlayerDiscoverAswangWeakness;
    }

    public void LoadPlayer()
    {
        if (SaveManager.instance.playerData.loadAllData)
        {
            //Load Position
            this.gameObject.transform.position = SaveManager.instance.playerData.lastSavePostion;
            this.gameObject.transform.eulerAngles = SaveManager.instance.playerData.lastSaveRotation;
        }
        //Load Inventory
        CollectBehaviour colBeh = this.GetComponent<CollectBehaviour>();
        //Copy Items from save
        List<Item> itemListCopyFSave = new List<Item>();
        for (int c = 0; c < SaveManager.instance.playerData.itemList.Count; c++)
        {
            itemListCopyFSave.Add(SaveManager.instance.playerData.itemList[c].ReturnDeepCopy());
        }
        colBeh.GetInventory().ChangeInvItemList(itemListCopyFSave);
        //////////////////
        ///Load PlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesLoad = new List<PlayerNotes>();
        for (int n = 0; n < SaveManager.instance.playerData.playerNotes.Count; n++)
        {
            pNotesLoad.Add(SaveManager.instance.playerData.playerNotes[n].ReturnDeepCopyNotes());
        }
        pData.pNotes = pNotesLoad;
        ///////////////////
        ///ManananggalObs
        List<AswangObservation> copyManang = new List<AswangObservation>();
        for (int m = 0; m < SaveManager.instance.playerData.mananananggalObs.Count; m++)
        {
            copyManang.Add(SaveManager.instance.playerData.mananananggalObs[m].ReturnDeepCopyObs());
        }
        manangObservations = copyManang;
        //AswangObs
        List<AswangObservation> copyAswang = new List<AswangObservation>();
        for (int a = 0; a < SaveManager.instance.playerData.aswangObs.Count; a++)
        {
            copyAswang.Add(SaveManager.instance.playerData.aswangObs[a].ReturnDeepCopyObs());
        }
        aswangObservations = copyAswang;

        Debug.Log("PlayerLoaded");
    }

    public void SavePlayer()
    {
        //
        SaveManager.instance.playerData.loadAllData = true;
        //Save Position
        SaveManager.instance.playerData.lastSavePostion = this.gameObject.transform.position;
        SaveManager.instance.playerData.lastSaveRotation = this.gameObject.transform.eulerAngles;
        //Getting Inventory.
        //Save Inventory
        CollectBehaviour colBeh = this.GetComponent<CollectBehaviour>();
        List<Item> itemListCopy = new List<Item>();
        for (int c = 0; c < colBeh.GetInventory().GetInventoryList().Count; c++)
        {
            itemListCopy.Add(colBeh.GetInventory().GetInventoryList()[c].ReturnDeepCopy());
        }
        SaveManager.instance.playerData.itemList = itemListCopy;
        ////////////////////
        ///SavePlayerNotes
        PlayersData pData = this.gameObject.GetComponent<PlayersData>();
        List<PlayerNotes> pNotesSave = new List<PlayerNotes>();
        for (int n = 0; n < pData.pNotes.Count; n++)
        {
            pNotesSave.Add(pData.pNotes[n].ReturnDeepCopyNotes());
        }
        SaveManager.instance.playerData.playerNotes = pNotesSave;
        ///////////////////
        ///ManananggalObs
        List<AswangObservation> copyManang = new List<AswangObservation>();
        for(int m =0; m<manangObservations.Count;m++)
        {
            copyManang.Add(manangObservations[m].ReturnDeepCopyObs());
        }
        SaveManager.instance.playerData.mananananggalObs = copyManang;
        //AswangObs
        List<AswangObservation> copyAswang = new List<AswangObservation>();
        for (int a = 0; a < aswangObservations.Count; a++)
        {
            copyAswang.Add(aswangObservations[a].ReturnDeepCopyObs());
        }
        SaveManager.instance.playerData.aswangObs = copyAswang;

        Debug.Log("PlayerSaved");
    }

    #region ManananggalObservation
    public void ManananggalSeePlayerLowHealth()
    {
        if (manangObservations.Count > 0)
        {
            for (int m = 0; m < manangObservations.Count; m++)
            {
                if (manangPlayLH.obsTitle == manangObservations[m].obsTitle)
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        manangObservations.Add(manangPlayLH);
    }
    public void PlayerDontSeeManangLowerBody()
    {
        if (manangObservations.Count > 0)
        {
            for (int m = 0; m < manangObservations.Count; m++)
            {
                if (manangPlayNoLB.obsTitle == manangObservations[m].obsTitle)
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        manangObservations.Add(manangPlayNoLB);
    }
    public void PlayerDiscoverMangWeakness()
    {
        if (manangObservations.Count > 0)
        {
            for (int m = 0; m < manangObservations.Count; m++)
            {
                if (mangPlayDiscoverWeak.obsTitle == manangObservations[m].obsTitle)
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        manangObservations.Add(mangPlayDiscoverWeak);
    }
    #endregion

    #region AswangObservation
    public void PlayerSawAswangGoInvi()
    {
        if (aswangObservations.Count > 0)
        {
            for (int m = 0; m < aswangObservations.Count; m++)
            {
                if (aswangInvisible.obsTitle == aswangObservations[m].obsTitle)
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        aswangObservations.Add(aswangInvisible);
    }    

    public void PlayerEnteredFamilyEstate()
    {
        if (aswangObservations.Count > 0)
        {
            for (int m = 0; m < aswangObservations.Count; m++)
            {
                if (aswangfamilyEstate.obsTitle == aswangObservations[m].obsTitle)
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        aswangObservations.Add(aswangfamilyEstate);
    }

    public void PlayerSawAswangEatingCorpses()
    {
        if (aswangObservations.Count > 0)
        {
            for (int m = 0; m < aswangObservations.Count; m++)
            {
                if (aswangEatCorpes.obsTitle == aswangObservations[m].obsTitle)
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        aswangObservations.Add(aswangEatCorpes);
    }

    public void PlayerDiscoverAswangWeakness()
    {
        if (aswangObservations.Count > 0)
        {
            for (int m = 0; m < aswangObservations.Count; m++)
            {
                if (aswangWeakness.obsTitle == aswangObservations[m].obsTitle)
                {
                    return;
                }
            }
        }
        PlayScribbleSound();
        aswangObservations.Add(aswangWeakness);
    }
    #endregion

    public void PlayScribbleSound()
    {
        //Referencer.Instance.journalUI.GetComponent<AudioSource>().clip = scribble;
        //Referencer.Instance.journalUI.GetComponent<AudioSource>().Play();
    }
}
