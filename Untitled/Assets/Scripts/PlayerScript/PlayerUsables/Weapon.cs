﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public enum GunType
    {
        PISTOL
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void Shoot()
    {
        Debug.Log("Shoot what?");
    }

    public virtual void Reload()
    {
        Debug.Log("Reload what?");
    }
}
