﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolProjectile : MonoBehaviour
{
    public static EventHandler OnEnemyHit;

    private float projectileSpeed = 3000f;
    private Transform player;
    [SerializeField] private Gun myGun;

    private Transform firePoint;
   


    private void Awake()
    {
        //player.Find("FollowTarget").transform.Find("WeaponSlot").transform.Find("Gun").GetComponent<Gun>();
        player = FindObjectOfType<ThirdPersonAimControl>().transform;
        myGun = player.transform.Find("WeaponSlot").transform.Find("WeaponHolder").transform.Find("WeaponPivot").GetComponentInChildren<Gun>();

        firePoint = myGun.transform.Find("firePoint");
    }


    // Start is called before the first frame update
    void Start()
    {
        Vector3 direction = myGun.GetProjectileDirection().position - firePoint.transform.position;

        transform.GetComponent<Rigidbody>().velocity = (firePoint.transform.forward + direction.normalized)  * (projectileSpeed * Time.deltaTime);

    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(myGun.GetFirePoint().position, transform.position) > myGun.range)
        {
            DestroyBullet();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            return;
        }
        else if(collision.gameObject.tag == "Enemy")
        {
            OnEnemyHit?.Invoke(this, EventArgs.Empty);
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }



    }

    public void DestroyBullet()
    {
        Destroy(gameObject);
    }
}
