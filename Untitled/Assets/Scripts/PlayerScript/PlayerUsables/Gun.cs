﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon, ILethal
{
    public static EventHandler OnGunShot;

    [SerializeField] private int _damage = 10;
    [SerializeField] private int _capacity = 10;
    [SerializeField] private int _ammo = 20;
    [SerializeField] private int _maxCapacity = 10;
    [SerializeField] private float _range = 50f;
    [SerializeField] private float _fireRate = 0.3f;
    [SerializeField] private Transform _firePoint;
    [Tooltip("CrosshairTarget Object")]
    [SerializeField] private Transform projectileDestination;
    [SerializeField] private GameObject _projectilePrefab;



    #region ILethal Property
    public int damage
    {
        get
        {
            return _damage;
        }
        set
        {
            _damage = value;
        }
    }
    public int capacity
    {
        get
        {
            return _capacity;
        }
        set
        {
            _capacity = value;
        }
    }
    public int ammo
    {
        get
        {
            return _ammo;
        }
        set
        {
            _ammo = value;
        }
    }
    public float range
    {
        get
        {
            return _range;
        }
        set
        {
            _range = value;
        }
    }
    public float fireRate
    {
        get
        {
            return _fireRate;
        }
        set
        {
            _fireRate = value;
        }
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Shoot()
    {
        Debug.Log("Shoot Pistol");
        if (capacity <= 0)
        {
            if(ammo > 0)
            {
                Reload();
            }

            return;
        }
        capacity--;
        var projectile = Instantiate(_projectilePrefab, _firePoint.position, Quaternion.identity);
        OnGunShot?.Invoke(this, EventArgs.Empty);
       
       
    }

    public override void Reload()
    {
        int ammoRequired; 
         
        if(capacity < 10)
        {
            ammoRequired = _maxCapacity - capacity;

            if(ammo < ammoRequired)
            {
                ammoRequired = ammo;
                capacity += ammoRequired;
                ammo -= ammoRequired;

            }
            else
            {
                capacity += ammoRequired;
                ammo -= ammoRequired;
            }


        }
    }

    public Transform GetFirePoint()
    {
        return _firePoint;
    }

    public void SetFirePoint(Transform location)
    {
        _firePoint = location;
    }

    public GameObject GetProjectile()
    {
        return _projectilePrefab;
    }

    public Transform GetProjectileDirection()
    {
        return projectileDestination;
    }

}
