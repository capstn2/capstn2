﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypeGoal : Goal
{
    Item.ItemType type;
    public ItemTypeGoal(string des, Item.ItemType tpe)
    {
        //objective = obj;
        description = des;
        type = tpe;
        Init();
    }

    public override void Init()
    {
        base.Init();
        ObjectiveEvent.Instance.onCheckItemType += CheckItemType;
        //Evaluate();
    }

    public void CheckItemType(Item.ItemType ty)
    {
        if (type == ty)
        {
            isCompleted = true;
            Debug.Log("Correct Quest ID");
        }
        ObjectiveEvent.Instance.CheckObjectives();
    }
}
