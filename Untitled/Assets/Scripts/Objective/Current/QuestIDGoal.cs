﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestIDGoal : Goal
{
    public int questID;
    public QuestIDGoal(string des, int id)
    {
        //objective = obj;
        description = des;
        questID = id;
        Init();
    }

    public override void Init()
    {
        base.Init();
        ObjectiveEvent.Instance.onQuestArrived += CheckQuestID;
        //Evaluate();
    }

    public void CheckQuestID(int id)
    {
        if (id == questID)
        {
            isCompleted = true;
            Debug.Log("Correct Quest ID");
        }
        ObjectiveEvent.Instance.CheckObjectives();
    }
}
