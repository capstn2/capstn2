﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ObjectiveSlot/NewGoal")]
public class SlotQGoal : ScriptableObject
{
    public GoalType type;
    [Header("Quest ID for Checking")]
    public int questID;

    [Header("What Item is Required")]
    public Item.ItemType requiredItem;
}

public enum GoalType
{
    quest,
    item
}