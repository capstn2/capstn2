﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveGiver : MonoBehaviour
{
    public ObjectiveSlot slot;
    void Start()
    {
        
    }

    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            ObjectiveEvent.Instance.AddObjectiveSlot(slot);
            Destroy(this.gameObject);
        }
    }
}
