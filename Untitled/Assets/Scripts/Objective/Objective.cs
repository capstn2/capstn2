﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Objective 
{
    public List<Goal> goals = new List<Goal>();
    public string ObjectiveName;
    public string ObjectiveDescription;
    public int objID;
    public ObjectiveSlot objSlot;

    public bool isCompleted;

    public Objective(ObjectiveSlot slot)
    {
        ObjectiveEvent.Instance.evalObjective += CheckObjective;
        objID = slot.objID;
        ObjectiveName = slot.objTitle;
        ObjectiveDescription = slot.objDescription;
        objSlot = slot;
        for (int i = 0; i < slot.goals.Count; i++)
        {
                    //ObjectiveEvent.Instance.UpdateSimpleObjUI(slot.objTitle);
                    //ObjectiveEvent.Instance.ConstructObjPageUI(this);
            switch(slot.goals[i].type)
            {
                case GoalType.quest:
                    goals.Add(new QuestIDGoal(slot.objDescription, slot.goals[i].questID));
                    break;
                case GoalType.item:
                    goals.Add(new ItemTypeGoal(slot.objDescription, slot.goals[i].requiredItem));
                    break;
            }
        }
    }


    public void CheckObjective()
    {
        isCompleted = goals.All(g => g.isCompleted);
        if(isCompleted)
        {
            isCompleted = true;
            ObjectiveEvent.Instance.CheckObjHandler();
            if(objSlot.chainedObjectiveSlot!=null)
            {
                ObjectiveEvent.Instance.AddObjectiveSlot(objSlot.chainedObjectiveSlot);
            }
        }
    }

}
