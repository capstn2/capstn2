﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

public enum ObjectiveType { Kill, Gather, PointAB }
public class ObjectiveHandler : MonoBehaviour
{
    public static ObjectiveHandler instance;

    public List<Objective> curObjectives;

    public ObjectiveSlot slot;
    public ObjectiveSlot slot2;

    public AudioClip scribble;
    //public Objective curObj;
    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        //ObjectiveEvent.Instance.evalObjHandler += CheckHandler;
        //GameEvents.Instance.onSave += SaveObjectives;
        ObjectiveEvent.Instance.addObjective += AddObjective;
        CheckIfExistingObjectives();
    }
    private void Update()
    {
        
        if(Input.GetKeyUp(KeyCode.Space))
        {
            ObjectiveEvent.Instance.OnQuestArrivedS(1);// for checking quest Id
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            ObjectiveEvent.Instance.OnCheckItemType(Item.ItemType.AlawigEssence);
        }

        if (Input.GetKeyUp(KeyCode.Backslash))
        {
            curObjectives.Add(new Objective(slot2));
        }

    }

    public void SaveObjectives()
    {
        List<Objective> copyObj = new List<Objective>(curObjectives);
        SaveManager.instance.playerData.playerObjective = copyObj;
    }

    public void CheckIfExistingObjectives()
    {
        if(SaveManager.instance.playerData.playerObjective.Count>0)
        {
            curObjectives = SaveManager.instance.playerData.playerObjective;
        }
    }
   
    public void AddObjective(ObjectiveSlot slot)
    {
        curObjectives.Add(new Objective(slot));
        this.GetComponent<AudioSource>().clip = scribble;
        this.GetComponent<AudioSource>().Play();
    }    

    public void CheckHandler()
    {
        
    }
}
