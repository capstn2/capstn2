﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointABGoal : Goal
{
    public int locID;
    public PointABGoal(string des,int id )
    {
        //objective = obj;
        description = des;
        locID = id;
        Init();
    }

    public override void Init()
    {
        base.Init();
        ObjectiveEvent.Instance.onArriveGoal += ArriveAtGoal;
        //Evaluate();
    }

    public void ArriveAtGoal(int id)
    {
        if (id == locID)
        {
            isCompleted = true;
            Debug.Log("Arrive at ID");
        }
        ObjectiveEvent.Instance.CheckObjectives();
    }

}
