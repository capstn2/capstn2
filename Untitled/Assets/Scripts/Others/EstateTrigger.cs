﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstateTrigger : MonoBehaviour
{
    public bool triggered = false;

    public void PlayerDiscoverAswangWeakness()
    {
        AswangObsEvent.Instance.OnPlayerDiscoveredAswangWeakness();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player"&&!triggered)
        {
            AswangObsEvent.Instance.OnPlayerEnterFamilyEstate();
            triggered = true;
        }
    }
}
