﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerQuestCheck : MonoBehaviour
{
    public int questId;
    public bool triggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player" && !triggered)
        {
            triggered = true;
            ObjectiveEvent.Instance.OnQuestArrivedS(questId);
        }
    }
}
