﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NoteItem : MonoBehaviour
{
    public PlayerNotes noteToGive;

    public UnityEvent OnPlayerGet;
    public void AddNoteToPlayer(GameObject player)
    {
        this.GetComponent<AudioSource>().Play();
        this.GetComponent<MeshRenderer>().enabled = false;
        this.GetComponent<BoxCollider>().enabled = false;
        player.GetComponent<PlayersData>().AddPlayerNotes(noteToGive);
        OnPlayerGet.Invoke();
        Destroy(this.gameObject,2f);
    }

    public void CallEventManangWeakness()
    {
        AswangObsEvent.Instance.OnPlayerDiscoverManananggalWeakness();
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.GetComponent<PlayersData>().AddPlayerNotes(noteToGive);
            Destroy(this.gameObject);
        }
    }*/
}
