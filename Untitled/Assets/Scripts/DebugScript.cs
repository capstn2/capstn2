﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugScript : MonoBehaviour
{
    public BoxCollider col;
    public Vector3 size;
    public Vector3 rSize;
    public Vector3 pos;
    public Vector3 rPos;
    void OnDrawGizmos()
    {
        //Gizmos.matrix = Matrix4x4.TRS(this.pos,this.transform.rotation,size);
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(pos, size);

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(rPos, size);
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        size = new Vector3(this.GetComponent<BoxCollider>().size.x * this.transform.localScale.x,
            this.GetComponent<BoxCollider>().size.z * this.transform.localScale.z,
            this.GetComponent<BoxCollider>().size.y * this.transform.localScale.y);
        rSize = new Vector3(this.GetComponent<BoxCollider>().size.x * this.transform.localScale.x,
            this.GetComponent<BoxCollider>().size.y * this.transform.localScale.y,
            this.GetComponent<BoxCollider>().size.z * this.transform.localScale.z);
        pos = this.transform.position + new Vector3(0, size.y + (this.GetComponent<BoxCollider>().center.z * this.transform.localScale.z), (this.GetComponent<BoxCollider>().center.y * this.transform.localScale.y) * -1);
        rPos = this.transform.position + new Vector3(0, this.GetComponent<BoxCollider>().center.z * this.transform.localScale.z, (this.GetComponent<BoxCollider>().center.y * this.transform.localScale.y) * -1);
    }
}
