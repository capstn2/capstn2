﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonMashPrompt : MonoBehaviour
{
    private Transform buttonMashContainer;
    private Transform image1;
    private Transform image2;

    // Start is called before the first frame update
    void Start()
    {
        ButtonMashControl.OnButtonMashFinished += ButtonMashControl_OnButtonMashFinished;
        SpecialSkill.OnManananggalSpecialFinished += SpecialSkill_OnManananggalSpecialFinished;
        SpecialSkill.OnManananggalSpecial += SpecialSkill_OnManananggalSpecial;
        buttonMashContainer = gameObject.transform.Find("ButtonMash");
        image1 = gameObject.transform.Find("ButtonMash").transform.Find("Image1");
        image2 = gameObject.transform.Find("ButtonMash").transform.Find("Image2");




    }

    void SpecialSkill_OnManananggalSpecial(object sender, System.EventArgs e)
    {

        buttonMashContainer.gameObject.SetActive(true);
    }


    void SpecialSkill_OnManananggalSpecialFinished(object sender, System.EventArgs e)
    {
        buttonMashContainer.gameObject.SetActive(false);
    }


    void ButtonMashControl_OnButtonMashFinished(object sender, System.EventArgs e)
    {
        buttonMashContainer.gameObject.SetActive(false);
    }


    private void OnEnable()
    {
        StartCoroutine(ButtonMashSequence());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator ButtonMashSequence()
    {
        while (true)
        {
           
            yield return new WaitForSeconds(.3f);

            if (image1.gameObject.active)
            {
                image1.gameObject.SetActive(false);
                image2.gameObject.SetActive(true);
            }
            else
            {
                image1.gameObject.SetActive(true);
                image2.gameObject.SetActive(false);
            }

        }

    }
}
