﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagUIScript : MonoBehaviour
{
    public GameObject unitReference;

    public GameObject healthBar;
    //public GameObject manaBar;

    //private ManaScript unitMana;
    private HealthScript unitHealth;
    private Material[] hpBarMat;
    //private Material[] mpBarMat;

    void Start()
    {
        hpBarMat = healthBar.GetComponent<MeshRenderer>().materials;
        //mpBarMat = manaBar.GetComponent<MeshRenderer>().materials;
        //unitMana = unitReference.GetComponent<ManaScript>();
        unitHealth = unitReference.GetComponent<HealthScript>();
    }

    void Update()
    {
        UpdateHealthBar();
        //UpdateManaBar();
    }

    /*
    private void UpdateManaBar()
    {
        float mp = unitMana.currentMana / unitMana.maxMana;
        float mpBar = (mp * 1.5f) + 5.80f;
        mpBarMat[1].SetFloat("Vector1_47D3A711", mpBar);
    }*/

    private void UpdateHealthBar()
    {
        float hp = unitHealth.curHealth / unitHealth.maxHealth;
        float temp = 1.9f * hp;
        float hpBar = -1.43f + temp;
        //hpBarMat[1].SetFloat("FRate", -0.48f);
        hpBarMat[0].SetFloat("FRate", hpBar);
    }
}
