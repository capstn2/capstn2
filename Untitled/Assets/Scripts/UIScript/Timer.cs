﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public GameManager gm;
    public Text txt;

    private void Update()
    {
        UpdateTimer();
    }
    
    public void UpdateTimer()
    {
        int min = Mathf.FloorToInt(gm.timeLeft / 60);
        int sec = Mathf.FloorToInt(gm.timeLeft % 60);
        txt.text = "Timer Left = " + min + ":" + sec;
    }
}
