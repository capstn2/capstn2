﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalTabs : MonoBehaviour
{
    public Image tabOn;
    public Image tabOff;
    public Image tabPoint;

    private bool isSelected = false;

    private Image butImage;
    private void Start()
    {
        butImage = this.GetComponent<Image>();
    }
    public void JournalTabOn()
    {
        tabOn.enabled = true;
        isSelected = true;
        tabOff.enabled = false;
        tabPoint.enabled = false;
    }

    public void JournalTabOff()
    {
        isSelected = false;
        tabOn.enabled = false;
        tabOff.enabled = true;
        tabPoint.enabled = false;
    }

    public void PointerOnTab()
    {
        if(!isSelected)
        {
            tabPoint.enabled = true;
            tabOn.enabled = false;
            tabOff.enabled = false;
        }
    }
    public void PointerOffTab()
    {
        tabPoint.enabled = false;
        if (isSelected)
        {
            tabOn.enabled = true;
            tabOff.enabled = false;
        }
        else
        {
            tabOn.enabled = false;
            tabOff.enabled = true;
        }
    }

    public void OnPointAswangPage()
    {
        tabPoint.enabled = true;
        tabOff.enabled = false;
    }
    public void OffPointAswangPage()
    {
        tabPoint.enabled = false;
        tabOff.enabled = true;
    }

}
