﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnGoingObjPage : MonoBehaviour
{
    public GameObject pointABTemplate;
    public GameObject killTemplate;
    public GameObject gatherTemplate;

    public ObjectiveHandler handler;
    private int curPage;
    void Start()
    {

    }

    void Update()
    {

    }
    public void ViewOnGoingObjectives(Objective obj)
    {
        //Old Journal Ui important
        /*
        switch (obj.objSlot.objType)
        {
            case ObjectiveType.PointAB:
                ChangeTemplate(0);
                ConstructPointPage(obj);
                break;
            case ObjectiveType.Kill:
                ChangeTemplate(1);
                ConstructPointPage(obj);
                break;
            case ObjectiveType.Gather:
                ChangeTemplate(2);
                ConstructPointPage(obj);
                break;
        }*/
    }

    public void ConstructPointPage(Objective obj)
    {
        if(!pointABTemplate.activeSelf)
        {
            Debug.LogError("pointABTemplate not Active");
        }
        PointABTemplateUI ui = pointABTemplate.GetComponent<PointABTemplateUI>();
        ui.title.text = obj.ObjectiveName;
        ui.description.text = obj.ObjectiveDescription;
        ui.isCompleted.text = obj.isCompleted.ToString();
    }
    public void ChangeTemplate(int i)
    {
        switch (i)
        {
            case 0:
                pointABTemplate.SetActive(true);
                killTemplate.SetActive(false);
                gatherTemplate.SetActive(false);
                break;
            case 1:
                pointABTemplate.SetActive(false);
                killTemplate.SetActive(true);
                gatherTemplate.SetActive(false);
                break;
            case 2:
                pointABTemplate.SetActive(false);
                killTemplate.SetActive(false);
                gatherTemplate.SetActive(true);
                break;
        }
    }
    public void PageChangeButton(int pageAdd)
    {
        if (handler.curObjectives.Count <= 0)
        {
            return;
        }
        else if (handler.curObjectives.Count == 1)
        {
            curPage = 0;
        }
        else if (handler.curObjectives.Count > 1)
        {
            if (curPage + pageAdd < 0)
            {
                curPage = handler.curObjectives.Count - 1;
            }
            else if (curPage + pageAdd > handler.curObjectives.Count - 1)
            {
                curPage = 0;
            }
            else
            {
                curPage += pageAdd;
            }
        }
        ChangePage(handler.curObjectives[curPage]);
    }
    public void ChangePage(Objective obj)
    {
        //old journal ui important
        /*
        switch (obj.objSlot.objType)
        {
            case ObjectiveType.PointAB:
                ChangeTemplate(0);
                ConstructPointPage(obj);
                break;
            case ObjectiveType.Kill:
                ChangeTemplate(1);
                ConstructPointPage(obj);
                break;
            case ObjectiveType.Gather:
                ChangeTemplate(2);
                ConstructPointPage(obj);
                break;
        }*/
    }
}