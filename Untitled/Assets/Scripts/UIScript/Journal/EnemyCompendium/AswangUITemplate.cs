﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AswangUITemplate : MonoBehaviour
{
   
    public Image aImage;
    public Text aName;
    public Text aDescription;

    public Text strength1;
    public Text strength2;
    public Text strength3;

    public Text weakness1;
    public Text weakness2;
    public Text weakness3;

    private string defString ="???????";
    
    // Update is called once per frame
    void Update()
    {
    }

    public void ConstructAswangTemplate(AswangInfo inf)
    {
        switch(inf.aswangID)
        {
            case 1:
                //CheckPlayerMananangalInfo(inf);
                break;
            case 2:
                //CheckPlayerMangkukulamInfo(inf);
                break;
            case 3:
                //CheckPlayerGhoulInfo(inf);
                break;
        }
    }
    /*
    public void CheckPlayerMananangalInfo(AswangInfo inf)
    {
        if(PlayerInfo.instance.mImageDiscover)
        {
            aImage.sprite = inf.aswangImage;
        }
        if (PlayerInfo.instance.mNameDiscover)
        {
            aName.text = inf.aswangName;
        }
        else
        {
            aName.text = defString;
        }
        if (PlayerInfo.instance.mDescriptionDiscover)
        {
            aDescription.text = inf.aswangDescription;
        }
        else
        {
            aDescription.text = defString;
        }
        if (PlayerInfo.instance.mStrengthDiscover1)
        {
            strength1.text = inf.strengths1;
        }
        else
        {
            strength1.text = defString;
        }
        if (PlayerInfo.instance.mStrengthDiscover2)
        {
            strength2.text = inf.strengths2;
        }
        else
        {
            strength2.text = defString;
        }
        if (PlayerInfo.instance.mStrengthDiscover3)
        {
            strength3.text = inf.strengths3;
        }
        else
        {
            strength3.text = defString;
        }
        if (PlayerInfo.instance.mWeaknessesDiscover1)
        {
            weakness1.text = inf.weakness1;
        }
        else
        {
            weakness1.text = defString;
        }
        if (PlayerInfo.instance.mWeaknessesDiscover2)
        {
            weakness2.text = inf.weakness2;
        }
        else
        {
            weakness2.text = defString;
        }
        if (PlayerInfo.instance.mWeaknessesDiscover3)
        {
            weakness3.text = inf.weakness3;
        }
        else
        {
            weakness3.text = defString;
        }
    }
    public void CheckPlayerMangkukulamInfo(AswangInfo inf)
    {
        if (PlayerInfo.instance.mangImageDiscover)
        {
            aImage.sprite = inf.aswangImage;
        }
        if (PlayerInfo.instance.mangNameDiscover)
        {
            aName.text = inf.aswangName;
        }
        else
        {
            aName.text = defString;
        }
        if (PlayerInfo.instance.mangDescriptionDiscover)
        {
            aDescription.text = inf.aswangDescription;
        }
        else
        {
            aDescription.text = defString;
        }
        if (PlayerInfo.instance.mangStrengthDiscover1)
        {
            strength1.text = inf.strengths1;
        }
        else
        {
            strength1.text = defString;
        }
        if (PlayerInfo.instance.mangStrengthDiscover2)
        {
            strength2.text = inf.strengths2;
        }
        else
        {
            strength2.text = defString;
        }
        if (PlayerInfo.instance.mangStrengthDiscover3)
        {
            strength3.text = inf.strengths3;
        }
        else
        {
            strength3.text = defString;
        }
        if (PlayerInfo.instance.mangWeaknessesDiscover1)
        {
            weakness1.text = inf.weakness1;
        }
        else
        {
            weakness1.text = defString;
        }
        if (PlayerInfo.instance.mangWeaknessesDiscover2)
        {
            weakness2.text = inf.weakness2;
        }
        else
        {
            weakness2.text = defString;
        }
        if (PlayerInfo.instance.mangWeaknessesDiscover3)
        {
            weakness3.text = inf.weakness3;
        }
        else
        {
            weakness3.text = defString;
        }
    }
    public void CheckPlayerGhoulInfo(AswangInfo inf)
    {
        if (PlayerInfo.instance.gImageDiscover)
        {
            aImage.sprite = inf.aswangImage;
        }
        if (PlayerInfo.instance.gNameDiscover)
        {
            aName.text = inf.aswangName;
        }
        else
        {
            aName.text = defString;
        }
        if (PlayerInfo.instance.gDescriptionDiscover)
        {
            aDescription.text = inf.aswangDescription;
        }
        else
        {
            aDescription.text = defString;
        }
        if (PlayerInfo.instance.gStrengthDiscover1)
        {
            strength1.text = inf.strengths1;
        }
        else
        {
            strength1.text = defString;
        }
        if (PlayerInfo.instance.gStrengthDiscover2)
        {
            strength2.text = inf.strengths2;
        }
        else
        {
            strength2.text = defString;
        }
        if (PlayerInfo.instance.gStrengthDiscover3)
        {
            strength3.text = inf.strengths3;
        }
        else
        {
            strength3.text = defString;
        }
        if (PlayerInfo.instance.gWeaknessesDiscover1)
        {
            weakness1.text = inf.weakness1;
        }
        else
        {
            weakness1.text = defString;
        }
        if (PlayerInfo.instance.gWeaknessesDiscover2)
        {
            weakness2.text = inf.weakness2;
        }
        else
        {
            weakness2.text = defString;
        }
        if (PlayerInfo.instance.gWeaknessesDiscover3)
        {
            weakness3.text = inf.weakness3;
        }
        else
        {
            weakness3.text = defString;
        }
    }
    */
}
