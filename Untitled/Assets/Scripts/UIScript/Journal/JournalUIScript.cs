﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JournalUIScript : MonoBehaviour
{
    [Header("New")]
    public GameObject aswangCanvas;
    public GameObject notesCanvas;
    public GameObject objectiveCanvas;
    public GameObject helpCanvas;

    public GameObject journalCanvas;

    [Header("JournalTabs")]
    public JournalTabs aswangTab;
    public JournalTabs notesTab;
    public JournalTabs objectiveTab;
    public JournalTabs helpTab;

    public bool isOpen = false;
    [Header("AudioSource")]
    public AudioClip turnPage;
    public AudioClip openJournal;
    public AudioClip closeJournal;

    void Start()
    {
        
        Referencer.Instance.journalUI = this;
        
        journalCanvas.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.J))
        {
            //OpenOrCloseJournal();
            ViewOrPutAwayJournal();
            Debug.Log("OpenOrCloseJournal");
        }
    }

    ///////////////////////////////New///////////////////////////////////
    public void OpenCanvasTab(int index)
    {
        this.GetComponent<AudioSource>().clip = turnPage;
        this.GetComponent<AudioSource>().Play();
        switch (index)
        {
            case 0:
                OpenAswangCanvas();
                CloseNotesCanvas();
                CloseObjectiveCanvas();
                CloseHelpCanvas();
                break;
            case 1:
                OpenNotesCanvas();
                CloseAswangCanvas();
                CloseObjectiveCanvas();
                CloseHelpCanvas();
                break;
            case 2:
                OpenObjectiveCanvas();
                CloseAswangCanvas();
                CloseNotesCanvas();
                CloseHelpCanvas();
                break;
            case 3:
                CloseObjectiveCanvas();
                CloseAswangCanvas();
                CloseNotesCanvas();
                OpenHelpCanvas();
                break;
        }
    }
    public void TurnOffButton(Button but)
    {

    }

    public void TurnPageSound()
    {
        this.GetComponent<AudioSource>().clip = turnPage;
        this.GetComponent<AudioSource>().Play();
    }

    public void ViewOrPutAwayJournal()
    {
        if(isOpen)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = 1;
            CloseAswangCanvas();
            journalCanvas.SetActive(false);
            isOpen = false;
            this.GetComponent<AudioSource>().clip = closeJournal;
            this.GetComponent<AudioSource>().Play();
        }
        else
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            journalCanvas.SetActive(true);
            isOpen = true;
            this.GetComponent<AudioSource>().clip = openJournal;
            this.GetComponent<AudioSource>().Play();
            OpenJournal();
        }
    }

    public void OpenJournal()
    {
        OpenAswangCanvas();
        CloseNotesCanvas();
        CloseObjectiveCanvas();
        CloseHelpCanvas();
    }
    public void OpenAswangCanvas()
    {
        aswangCanvas.SetActive(true);
        aswangTab.JournalTabOn();
        aswangCanvas.GetComponent<AswangCanvas>().ShowOrCloseAswangJournal();
    }
    public void CloseAswangCanvas()
    {
        aswangCanvas.SetActive(false);
        aswangTab.JournalTabOff();
        aswangCanvas.GetComponent<AswangCanvas>().ResetNotesCanvas();
    }
    public void OpenNotesCanvas()
    {
        notesCanvas.SetActive(true);
        notesTab.JournalTabOn();
        notesCanvas.GetComponent<NotesCanvas>().ShowOrCloseNoteJournal();
    }
    public void CloseNotesCanvas()
    {
        notesCanvas.SetActive(false);
        notesTab.JournalTabOff();
        notesCanvas.GetComponent<NotesCanvas>().ResetNotesCanvas();
    }
    public void OpenObjectiveCanvas()
    {
        objectiveCanvas.SetActive(true);
        objectiveTab.JournalTabOn();
        objectiveCanvas.GetComponent<ObjectiveJCanvas>().ShowOrCloseObjectiveJournal();
    }
    public void CloseObjectiveCanvas()
    {
        objectiveCanvas.SetActive(false);
        objectiveTab.JournalTabOff();
        objectiveCanvas.GetComponent<ObjectiveJCanvas>().ResetObjectiveCanvas();
    }

    public void OpenHelpCanvas()
    {
        helpTab.JournalTabOn();
        helpCanvas.SetActive(true);
    }

    public void CloseHelpCanvas()
    {
        helpTab.JournalTabOff();
        helpCanvas.SetActive(false);
    }
}
