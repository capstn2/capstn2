﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesCanvas : MonoBehaviour
{

    public List<PlayerNotes> notes;
    public PlayersData playDat;
    [Header("NeedReference")]
    public GameObject noteButton;// prefab of note buttons;
    public GameObject noteJournal; // reference of the whole note journl
    public Text notesContent; // text reference of the content note
    public Text notesTitle; // text reference of the note title
    public Transform notesContainer; // reference of the scrollable container

    public bool isOpen;

    public List<GameObject> noteButList;

    void Start()
    {
        if(noteButton == null||noteJournal==null||notesContent == null|| notesTitle == null||notesContainer == null||playDat==null)
        {
            Debug.LogError("Notes Canvas is Missing Reference/s");
        }
        else
        {
            isOpen = false;
            //noteJournal.SetActive(false);
        }
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.J))
        {
            //ShowOrCloseNoteJournal();
        }
    }

    public void ShowOrCloseNoteJournal()
    {
        if(isOpen)
        {
            ClearNoteButList();
            CloseNotesCanvas();
        }
        else
        {
            //noteJournal.SetActive(true);
            ViewPlayerNotes();
            isOpen = true;
        }
    }

    public void ViewPlayerNotes()
    {
        if(playDat.pNotes.Count>0)
        {
            notesContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(notesContainer.GetComponent<RectTransform>().sizeDelta.x, playDat.pNotes.Count * 75);
            for (int i = 0;i< playDat.pNotes.Count;i++)
            {
                GameObject gam = Instantiate(noteButton, notesContainer);
                gam.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, i * -75, 0);
                Debug.Log(gam.GetComponent<NotesButton>() + "  " + playDat.pNotes[i].noteTitle);
                gam.GetComponent<NotesButton>().title = playDat.pNotes[i].noteTitle; //send the title to display
                gam.GetComponent<NotesButton>().notesContentUI = notesContent; // reference the text component
                gam.GetComponent<NotesButton>().content = playDat.pNotes[i].noteContent;// send the note content
                gam.GetComponent<NotesButton>().notesTitleUI = notesTitle; // send title text reference
                gam.GetComponent<NotesButton>().notesButTitleUI.text = playDat.pNotes[i].noteButTitle;
                if(playDat.pNotes[i].objSlot != null)
                {
                    gam.GetComponent<NotesButton>().oSlot = playDat.pNotes[i].objSlot;
                }

                noteButList.Add(gam);
            }
        }
    }
    public void ClearNoteButList()
    {
        if (noteButList.Count > 0)
        {
            for (int d = 0; d < noteButList.Count; d++)
            {
                Destroy(noteButList[d].gameObject);
            }
            noteButList.Clear();
        }
    }
    public void CloseNotesCanvas()
    {

        isOpen = false;
        //noteJournal.SetActive(false);
    }
    //Function for resetting notes canvas, used for chaning tabs
    public void ResetNotesCanvas()
    {
        ClearNoteButList();
        CloseNotesCanvas();
    }
}
