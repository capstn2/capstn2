﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesButton : MonoBehaviour
{
    public Text notesButTitleUI; // button title of note

    [HideInInspector] public Text notesContentUI;// the content of note
    [HideInInspector] public Text notesTitleUI; // the title of note
    [HideInInspector] public string content;
    [HideInInspector] public string title;
    [HideInInspector] public ObjectiveSlot oSlot; // for getting quest

    public void OnButtonClick()
    {
        notesContentUI.text = content;
        notesTitleUI.text = title;
        if(oSlot !=null)
        {
            ObjectiveHandler.instance.AddObjective(oSlot);
        }
    }
}
