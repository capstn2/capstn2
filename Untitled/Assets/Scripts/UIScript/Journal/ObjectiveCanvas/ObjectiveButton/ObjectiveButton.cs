﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveButton : MonoBehaviour
{
    public Text objectiveButTitleUI; // button title of note

    [HideInInspector] public Text objectiveContentUI;// the content of note
    [HideInInspector] public Text objectiveTitleUI; // the title of note
    [HideInInspector] public GameObject comp; // the title of note
    [HideInInspector] public string content;
    [HideInInspector] public string title;
    [HideInInspector] public bool isCompleted = false;

    public void OnButtonClick()
    {
        objectiveContentUI.text = content;
        objectiveTitleUI.text = title;
        if(isCompleted)
        {
            comp.SetActive(true);
        }
        else
        {
            comp.SetActive(false);
        }
    }
}
