﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum WhatKey
{
    Flashlight,
    FlashLightReload,
    Journal,
    Inventory,
    Interact
}

public class InstructionScript : MonoBehaviour
{
    public List<KeyCode> keysToDestroy;
    public GameObject txtCanvas;
    [TextArea(10,20)]
    public string theInstruction;
    private bool isActive;
    public InstructionScript chainInstruction;
    public HotkeyManager hkManager;
    public WhatKey keyW;
    private void Start()
    {
        isActive = false;
    }
    void Update()
    {
        if (isActive)
        {
            foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(vKey))
                {
                    for (int i = 0; i < keysToDestroy.Count; i++)
                    {
                        if (vKey == keysToDestroy[i])
                        {
                            keysToDestroy.RemoveAt(i);
                        }
                    }
                }
            }
             
            if (keysToDestroy.Count <= 0)
            {
                if(chainInstruction!=null)
                {
                    chainInstruction.gameObject.SetActive(true);
                }
                txtCanvas.SetActive(false);
                Destroy(this.gameObject);
            }
        }

    }

    public void DisplayInstruction()
    {
        if (hkManager != null)
        {
            switch (keyW)
            {
                case WhatKey.Flashlight:
                    hkManager.canUseFlashLight = true;
                    break;
                case WhatKey.FlashLightReload:
                    hkManager.canRealodFlashlight = true;
                    break;
                case WhatKey.Interact:
                    hkManager.canInteract = true;
                    break;
                case WhatKey.Inventory:
                    hkManager.canUseInventory = true;
                    break;
                case WhatKey.Journal:
                    hkManager.canUseJournal = true;
                    break;
            }
        }
        txtCanvas.SetActive(true);
        txtCanvas.GetComponent<Text>().text = theInstruction;
    }
    public void MakeIsActive()
    {
        isActive = true;
    }
    public void DestroyInstruction()
    {
        txtCanvas.SetActive(false);
        Destroy(this.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            isActive = true; 
            DisplayInstruction();
        }
    }
}
