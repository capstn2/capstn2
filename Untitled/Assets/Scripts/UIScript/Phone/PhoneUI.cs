﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneUI : MonoBehaviour
{
    public Image batteryFill;

    public PhoneScript phone;
    private void Awake()
    {
        Referencer.Instance.phoneUI = this;
    }
    void Start()
    {
    }

    void Update()
    {
        
    }

    public void UpdateBatteryUI(float cur, float max)
    {
        batteryFill.fillAmount = cur / max;
    }
}
