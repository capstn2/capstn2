﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InteractablePromptUI : MonoBehaviour
{
    public static InteractablePromptUI Instance { get; private set; }

    private TextMeshProUGUI textMeshPro;


    private void Awake()
    {
        Instance = this;
        textMeshPro = transform.Find("text").GetComponent<TextMeshProUGUI>();

        Hide();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void SetText(string itemTipText)
    {
        textMeshPro.SetText(itemTipText);
        textMeshPro.ForceMeshUpdate();

        Vector2 textSize = textMeshPro.GetRenderedValues(false);
        Vector2 padding = new Vector2(10, 10);
    }

    public void Show(string itemTipText)
    {
        gameObject.SetActive(true);
        SetText(itemTipText);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
