﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingBehvaiour : MonoBehaviour
{
    private MeshRenderer meshRenderer;
    private float disppearTimer;
    private float disappearTimerMax;
    private Color color;

    private void Awake()
    {
        meshRenderer = transform.Find("ping").GetComponent<MeshRenderer>();
        disappearTimerMax = 1f;
        disppearTimer = 0f;
        color = new Color(1, 1, 1, 1f);
    }

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        disappearTimerMax -= Time.deltaTime;
        color.a = Mathf.Lerp(disappearTimerMax, 0f, disppearTimer / disappearTimerMax);
        meshRenderer.material.color = color;

        if(disppearTimer >= disappearTimerMax)
        {
            Destroy(gameObject);
        }
    }

    public void SetColor(Color color)
    {
        this.color = color;
    }

    public void SetDisappearTimer(float disappearTimerMax)
    {
        this.disappearTimerMax = disappearTimerMax;
        disppearTimer = 0f;
    }
}
