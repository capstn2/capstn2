﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "CraftItem")]
public class ItemToCraft : ScriptableObject
{
    public int craftID;
    public string cName;
    [TextArea(5,10)]
    public string cDescription;
    public Item.ItemType scrollType;

    public List<CraftIngredients> cIngredients;
}
