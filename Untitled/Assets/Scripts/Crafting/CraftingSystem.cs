﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CraftingSystem : MonoBehaviour
{
    private Inventory playerInventory;
    private CollectBehaviour colBeh;
    public List<ItemToCraft> craftItems;

    public List<InteractableTypeSO> craftItemTemplate;

    private void Start()
    {
        colBeh = transform.GetComponent<CollectBehaviour>();
        playerInventory = colBeh.GetInventory();
    }
    void Update()
    {
        /*
        if(Input.GetKeyUp(KeyCode.L))
        {
            Item cItem = new Item();
            cItem.interactable = craftItemTemplate[0];
            playerInventory.AddItem(cItem);
            Item cItem2 = new Item();
            cItem2.interactable = craftItemTemplate[5];
            playerInventory.AddItem(cItem2);
        }*/

    }

    public Inventory ReturnInventory()
    {
        return playerInventory;
    }

    public void CraftItem(int id)
    {
        for (int i = 0; i<craftItems.Count;i++)
        {
            if(id == craftItems[i].craftID)
            {
                if(AbleToCraft(craftItems[i]))
                {
                    RemoveIngredientsForCraft(craftItems[i]);
                    Item cItem = new Item();
                    cItem.interactable = ReturnCorrectInteractableSO(craftItems[i]);
                    playerInventory.AddItem(cItem);
                }
            }
        }
    }
    public InteractableTypeSO ReturnCorrectInteractableSO(ItemToCraft craft)
    {
        for (int x = 0; x < craftItemTemplate.Count; x++)
        {
            if(craftItemTemplate[x].itemType == craft.scrollType)
            {
                return craftItemTemplate[x];
            }
        }

        return null;
        
    }
    public void RemoveIngredientsForCraft(ItemToCraft craft)
    {
        int counter = 0;
        //Debug.Log("Crafting : "+ craft.name+" For this much : "+craft.ingredients.Count);
        for (int x = 0; x < craft.cIngredients.Count; x++)
        {
            for (int y = 0; y < playerInventory.GetInventoryList().Count; y++)
            {
                if (craft.cIngredients[x].ingredient == playerInventory.GetInventoryList()[y].interactable.itemType)
                {
                    for(int z = 0;z< craft.cIngredients[x].amount;z++)
                    {
                        playerInventory.RemoveItem(playerInventory.GetInventoryList()[y]);
                    }
                    counter++;
                    if(counter == craft.cIngredients.Count)
                    {
                        return;
                    }
                }
            }
        }
    }
    
    public bool AbleToCraft(ItemToCraft craft)
    {
        //Need Checkers for something that is stackable(Check), non- stackable and stackable but with limit;
        List<Item.ItemType> need = new List<Item.ItemType>(); //Empty list that will we filled out throughout the loop, its the checker if player has sufficient ingredients;
        List<Item> itemListCopy = new List<Item>(); //this is the deep copied of the inventory, some items will be remove for some purposes.

        for (int c = 0; c < playerInventory.GetInventoryList().Count; c++)
        {
            itemListCopy.Add(playerInventory.GetInventoryList()[c].ReturnDeepCopy());
        }
        for (int x = 0; x< craft.cIngredients.Count;x++)
        {
            for(int y = 0;y< itemListCopy.Count;y++)
            {
                if(craft.cIngredients[x].ingredient == itemListCopy[y].interactable.itemType)
                {
                    if (itemListCopy[y].interactable.isStackable)
                    {
                        if (craft.cIngredients[x].amount <= itemListCopy[y].amount)
                        {
                            need.Add(itemListCopy[y].interactable.itemType);
                        }
                    }
                    else if(!itemListCopy[y].interactable.isStackable)
                    {
                        int counter = 0;
                        for (int z = 0; z < itemListCopy.Count; z++) 
                        {
                            if(craft.cIngredients[x].ingredient == itemListCopy[z].interactable.itemType)
                            {
                                counter++;
                            }
                        }
                        if(counter>= craft.cIngredients[x].amount)
                        {
                            need.Add(itemListCopy[y].interactable.itemType);
                        }
                    }
                }
            }
        }
        //Debug.Log("Need count : "+need.Count);
        //Debug.Log("Craft count : " + craft.cIngredients.Count);
        if (need.Count >= craft.cIngredients.Count)
        {
            //Debug.Log("CraftingTrue");
            return true;
        }
        else
        {
            //Debug.Log("CraftingFalse");
            return false;
        }
    }
}
