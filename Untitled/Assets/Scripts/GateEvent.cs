﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateEvent : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        Die.OnDie += (object sender, System.EventArgs e) =>
        {
            Destroy(gameObject);
        };


    }

    // Update is called once per frame
    void Update()
    {

    }
}
