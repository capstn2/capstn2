﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialogue/DialogueSet")]
public class DialogueSet : ScriptableObject
{
    public List<Dialogue> dialogueSet = new List<Dialogue>();
    private int currentItem;

    public void NextItem()
    {
        currentItem++;
    }
    public int CurItem()
    {
        return currentItem;
    }

    public int TotalDialogue()
    {
        return dialogueSet.Count;
    }
}
