﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogueGiver : MonoBehaviour
{
    public DialogueSet dialSet;
    public UnityEvent OnFinishDialogue;

    private void Update()
    {
        if(Input.GetKeyUp(KeyCode.Z))
        {
            SignalToActivate(dialSet);
        }
    }
    public void SignalToActivate(DialogueSet dialSetTL)
    {
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(true);
        dial.ActivateDialogue(dialSetTL);
        dial.OnDialogueComplete.AddListener(OnDialogueComplete);
    }
    public void OnDialogueComplete()
    {
        OnFinishDialogue.Invoke();
        DialogueSystem dial = DialogueSystem.instance;
        Disable();
        dial.OnDialogueComplete.RemoveListener(OnDialogueComplete);
    }
    public void Disable()
    {
        Debug.Log("Trigger = " + this.gameObject.name);
        DialogueSystem dial = DialogueSystem.instance;
        dial.gameObject.SetActive(false);
    }
}
