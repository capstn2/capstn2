﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseSound : MonoBehaviour
{
    private bool isActive = false;
    private float audioVolume;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        Chase.OnChasePlayer += Chase_OnChasePlayer;
        Chase.OnEndChase += Chase_OnEndChase;
        Stun.OnStunFinish += Stun_OnStunFinish;
    }

    void Stun_OnStunFinish(object sender, System.EventArgs e)
    {
        if(!isActive)
        { return; }
        isActive = false;
    }


    void Chase_OnEndChase(object sender, System.EventArgs e)
    {
        if (!isActive)
        {
            return;
        }
        isActive = false;

    }


    void Chase_OnChasePlayer(object sender, System.EventArgs e)
    {
        if (isActive)
        {
            return;
        }
        isActive = true;
    }


    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            audioSource.volume += Time.deltaTime * .2f;
        }
        else
        {
            audioSource.volume -= Time.deltaTime * .2f;
        }
    }
}
